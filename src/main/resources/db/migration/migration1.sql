CREATE TABLE admins
(
    id           INT AUTO_INCREMENT NOT NULL,
    img          LONGTEXT    NOT NULL,
    name         VARCHAR(50) NOT NULL,
    sex          VARCHAR(10) NOT NULL,
    dob          date        NOT NULL,
    phone        VARCHAR(10) NOT NULL,
    email        VARCHAR(50) NOT NULL,
    password     VARBINARY(500) NOT NULL,
    `role`       VARCHAR(20) NOT NULL,
    address      LONGTEXT NULL,
    job          LONGTEXT NULL,
    workplace    LONGTEXT NULL,
    state        VARCHAR(10) NOT NULL,
    created_by   VARCHAR(50) NOT NULL,
    created_date date        NOT NULL,
    updated_by   VARCHAR(50) NOT NULL,
    updated_date date        NOT NULL,
    CONSTRAINT pk_admins PRIMARY KEY (id)
);

CREATE TABLE classes
(
    id             INT AUTO_INCREMENT NOT NULL,
    full_name      LONGTEXT    NOT NULL,
    name           VARCHAR(50) NOT NULL,
    how_to_learn   VARCHAR(10) NOT NULL,
    number_student INT         NOT NULL,
    date_opening   date        NOT NULL,
    teacher_name   VARCHAR(50) NOT NULL,
    manager_id     INT         NOT NULL,
    supporter_id   INT         NOT NULL,
    course_id      INT         NOT NULL,
    special        LONGTEXT NULL,
    state          VARCHAR(10) NOT NULL,
    created_by     VARCHAR(50) NOT NULL,
    created_date   date        NOT NULL,
    updated_by     VARCHAR(50) NOT NULL,
    updated_date   date        NOT NULL,
    CONSTRAINT pk_classes PRIMARY KEY (id)
);

CREATE TABLE courses
(
    id               INT AUTO_INCREMENT NOT NULL,
    img              LONGTEXT    NOT NULL,
    name             VARCHAR(50) NOT NULL,
    `desc`           LONGTEXT    NOT NULL,
    content          LONGTEXT    NOT NULL,
    price            DOUBLE      NOT NULL,
    total_hour_study INT         NOT NULL,
    type             LONGTEXT    NOT NULL,
    state            VARCHAR(10) NOT NULL,
    created_by       VARCHAR(50) NOT NULL,
    created_date     date        NOT NULL,
    updated_by       VARCHAR(50) NOT NULL,
    updated_date     date        NOT NULL,
    CONSTRAINT pk_courses PRIMARY KEY (id)
);

CREATE TABLE jobs
(
    id          INT AUTO_INCREMENT NOT NULL,
    password    VARBINARY(500) NOT NULL,
    img         LONGTEXT    NOT NULL,
    name        LONGTEXT    NOT NULL,
    `desc`      LONGTEXT    NOT NULL,
    contact     VARCHAR(50) NULL,
    phone       VARCHAR(10) NULL,
    email       VARCHAR(50) NULL,
    address     LONGTEXT NULL,
    content     LONGTEXT    NOT NULL,
    date_posted date        NOT NULL,
    type        VARCHAR(45) NOT NULL,
    CONSTRAINT pk_jobs PRIMARY KEY (id)
);

CREATE TABLE media_components
(
    id   INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(50) NOT NULL,
    url  LONGTEXT    NOT NULL,
    CONSTRAINT pk_media_components PRIMARY KEY (id)
);

CREATE TABLE news
(
    id           INT AUTO_INCREMENT NOT NULL,
    img          LONGTEXT    NOT NULL,
    name         LONGTEXT    NOT NULL,
    `desc`       LONGTEXT    NOT NULL,
    text         LONGTEXT    NOT NULL,
    poster_id    INT         NOT NULL,
    type         VARCHAR(45) NOT NULL,
    source       LONGTEXT NULL,
    state        VARCHAR(10) NOT NULL,
    created_by   VARCHAR(50) NOT NULL,
    created_date date        NOT NULL,
    updated_by   VARCHAR(50) NOT NULL,
    updated_date date        NOT NULL,
    CONSTRAINT pk_news PRIMARY KEY (id)
);

CREATE TABLE users
(
    id           INT AUTO_INCREMENT NOT NULL,
    img          LONGTEXT    NOT NULL,
    name         VARCHAR(50) NOT NULL,
    sex          VARCHAR(10) NOT NULL,
    dob          date        NOT NULL,
    phone        VARCHAR(10) NOT NULL,
    email        VARCHAR(50) NOT NULL,
    address      LONGTEXT NULL,
    job          LONGTEXT NULL,
    workplace    LONGTEXT NULL,
    top_student  BIT(1)      NOT NULL,
    feedback     LONGTEXT NULL,
    state        VARCHAR(10) NOT NULL,
    created_by   VARCHAR(50) NOT NULL,
    created_date date        NOT NULL,
    updated_by   VARCHAR(50) NOT NULL,
    updated_date date        NOT NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE users_classes_admins
(
    id              INT AUTO_INCREMENT NOT NULL,
    class_id        INT         NOT NULL,
    collaborator_id INT NULL,
    student_id      INT         NOT NULL,
    state           VARCHAR(10) NOT NULL,
    created_by      VARCHAR(50) NOT NULL,
    created_date    date        NOT NULL,
    updated_by      VARCHAR(50) NOT NULL,
    updated_date    date        NOT NULL,
    CONSTRAINT pk_users_classes_admins PRIMARY KEY (id)
);

ALTER TABLE classes
    ADD CONSTRAINT FK_CLASSES_ON_COURSE FOREIGN KEY (course_id) REFERENCES courses (id);

ALTER TABLE classes
    ADD CONSTRAINT FK_CLASSES_ON_MANAGER FOREIGN KEY (manager_id) REFERENCES admins (id);

ALTER TABLE classes
    ADD CONSTRAINT FK_CLASSES_ON_SUPPORTER FOREIGN KEY (supporter_id) REFERENCES admins (id);

ALTER TABLE news
    ADD CONSTRAINT FK_NEWS_ON_POSTER FOREIGN KEY (poster_id) REFERENCES admins (id);

ALTER TABLE users_classes_admins
    ADD CONSTRAINT FK_USERS_CLASSES_ADMINS_ON_CLASS FOREIGN KEY (class_id) REFERENCES classes (id);

ALTER TABLE users_classes_admins
    ADD CONSTRAINT FK_USERS_CLASSES_ADMINS_ON_COLLABORATOR FOREIGN KEY (collaborator_id) REFERENCES admins (id);

ALTER TABLE users_classes_admins
    ADD CONSTRAINT FK_USERS_CLASSES_ADMINS_ON_STUDENT FOREIGN KEY (student_id) REFERENCES users (id);