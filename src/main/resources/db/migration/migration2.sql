ALTER TABLE admins MODIFY password varbinary(500) not null;
INSERT INTO `admins` (id, img, name, sex, dob, phone, email, password, role, address, job, workplace, state, created_by, created_date, updated_by, updated_date)
VALUES (1,
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649214207/admin_user/unknow_user.jpg',
        'Admin', 'Female', '1999-01-01', '0987654321', 'hvact@gmail.com',
        _binary '$2a$12$GyQFHA3dE82TMj/DD0k10esrds0x44g.3cogGTbR9ViMGlWAscwSW', 'Owner', 'Hà Nội',
        'Sinh viên', 'Học viện Kỹ thuật Mật mã', 'ACTIVE', 'Nguyễn Tiến Đạt', '2021-03-20',
        'Nguyễn Tiến Đạt', '2022-04-08'),
       (2,
        'https://res.cloudinary.com/dralhpdiv/image/upload/admin_user/daide1392@gmail.com_1649214375565_1649214375565',
        'Nguyễn Tiến Đạt', 'Male', '2000-09-13', '0383777790', 'daide1392@gmail.com',
        _binary '$2a$10$2r8Ycv.3FvE14dXV2DkeNOf51As6cvA7r5iffazZJbAEt8yILp9jC', 'Collaborator',
        'Hà Nội', 'Sinh viên', 'Học viện Kỹ thuật Mật mã', 'ACTIVE', 'Nguyễn Tiến Đạt',
        '2021-03-20', 'Nguyễn Tiến Đạt', '2022-04-08'),
       (3,
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649214207/admin_user/unknow_user.jpg',
        'Phạm Phương Anh', 'Female', '1999-01-01', '0987654321', 'phuonganh@hocvienact.edu.vn',
        _binary '$2a$12$GyQFHA3dE82TMj/DD0k10esrds0x44g.3cogGTbR9ViMGlWAscwSW', 'Manager', 'Hà Nội',
        'Sinh viên', 'Học viện Kỹ thuật Mật mã', 'ACTIVE', 'Nguyễn Tiến Đạt', '2021-03-20',
        'Nguyễn Tiến Đạt', '2022-04-08');

INSERT INTO `users`(id, img, name, sex, dob, phone, email, address, job, workplace, top_student, feedback, state, created_by, created_date, updated_by, updated_date)
VALUES (1,
        'https://res.cloudinary.com/tiendat-1392-hvact/image/upload/v1630211437/user/nxphong.png',
        'Nguyễn Xuân Phong', 'Male', '2000-01-01', '0383779980', 'phong@gmail.com', 'Hà Nội',
        'Chuyên viên quản trị và bảo mật hệ thống', 'Tập đoàn VinGroup', 1, '', 'ACTIVE',
        'Nguyễn Tiến Đạt', '2021-03-20', 'Nguyễn Tiến Đạt', '2022-04-04'),
       (2, 'https://res.cloudinary.com/tiendat-1392-hvact/image/upload/v1630211437/user/lananh.png',
        'Khổng Thị Lan Anh', 'Female', '2000-01-01', '0383779980', 'lananh@gmail.com', 'Hà Nội',
        'Chuyên viên kiểm thử phần mềm', 'FPT Corporation', 1, '', 'ACTIVE', 'Nguyễn Tiến Đạt',
        '2021-03-20', 'Nguyễn Tiến Đạt', '2022-04-04'),
       (3,
        'https://res.cloudinary.com/tiendat-1392-hvact/image/upload/v1630211437/user/minhduy.png',
        'Bùi Duy Minh', 'Male', '2000-01-01', '0383779980', 'minh@gmail.com', 'Hà Nội',
        'Chuyên viên quản trị hệ thống', 'ECOIT', 1, '', 'ACTIVE', 'Nguyễn Tiến Đạt', '2021-03-20',
        'Nguyễn Tiến Đạt', '2022-04-04');

INSERT INTO `media_components`(id, name, url)
VALUES (1, 'video_banner',
        'https://res.cloudinary.com/dralhpdiv/video/upload/v1649738315/media/Banner.mp4'),
       (2, 'slider_01',
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649738673/media/slider_01.jpg'),
       (3, 'slider_02',
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649738638/media/slider_02.jpg'),
       (4, 'slider_03',
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649752340/media/slider_03.jpg'),
       (5, 'slider_04',
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649752340/media/slider_04.jpg'),
       (6, 'slider_05',
        'https://res.cloudinary.com/dralhpdiv/image/upload/v1649752340/media/slider_05.jpg');

INSERT INTO `news`(id, img, name, `desc`, text, poster_id, type, source, state, created_by, created_date, updated_by, updated_date)
VALUES (1,
        'https://res.cloudinary.com/dralhpdiv/image/upload/c_scale,h_162,w_260/v1646968828/news/unknow_news.jpg',
        'Lịch sử phát triển', 'Lịch sử phát triển',
        '<h1>Lịch sử phát triển</h1> <br><h5 style=\"color: rgb(7, 7, 115);\">GIỚI THIỆU</h5> <ul> <li>Học viện Công nghệ thông tin ACT là sự hợp tác chính thức của Học viện Kỹ thuật Mật mã với các tập đoàn Cisco Systems, Microsoft trong lĩnh vực đào tạo, nghiên cứu, triển khai Công nghệ thông tin (CNTT) và An toàn thông tin.</li><li>Học viện CNTT ACT được xây dựng và phát triển bởi các Chuyên gia CNTT hàng đầu Việt Nam, là nhà tiên phong trong lĩnh vực đào tạo và cung cấp các chương trình đào tạo từ cơ bản đến nâng cao về Công nghệ thông tin và An toàn thông tin theo chuẩn quốc tế.</li><li>Chúng tôi cung cấp các dịch vụ tư vấn, đào tạo cũng như các giải pháp phần mềm và hệ thống thông tin chuyên nghiệp cho các cơ quan chính phủ, tổ chức, các công ty và cá nhân làm việc trong ngành CNTT.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\">SỨ MỆNH</h5> <ul> <li>Đồng hành, hoàn thiện và phát triển sự nghiệp bền vững, lâu dài của từng học viên, tiến đến mục tiêu xa hơn là đưa Việt Nam sớm trở thành một đất nước mạnh về CNTT nói chung và An toàn thông tin nói riêng.</li><li>Sự khác biệt của Học viện CNTT ACT là đổi mới sáng tạo, đào tạo theo hình thức liên kết chặt chẽ với các doanh nghiệp CNTT, gắn đào tạo với thực tiễn, với nghiên cứu – triển khai và các công nghệ hiện đại nhất.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\">TẦM NHÌN</h5> <ul> <li>Học viện CNTT ACT là nhà cung cấp nguồn lực,giải pháp dạy và học CNTT, An toàn thông tin hàng đầu tại Việt Nam.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\">VĂN HÓA</h5> <ul> <li>Học viện CNTT ACT hoạt động dựa trên nguyên tắc lấy Học viên và Khách hàng làm kim chỉ nam để xây dựng và phát triển lâu dài.</li><li>Chúng tôi luôn tuân thủ đầy đủ các nguyên tắc trong quản lý và đào tạo Đặt hết tâm huyết vào sự thành công của Học viên và khách hàng.</li><li>Nhiệt huyết, tận tâm, luôn sẵn sàng tư vấn và giải đáp mọi thắc mắc.</li><li>Tuyển chọn và hợp tác với những chuyên gia hàng đầu về CNTT tại Việt Nam.</li><li>Hỗ trợ Học viên có hoàn cảnh khó khăn, gia đình chính sách trên mọi phương diện.</li><li>Lấy tinh thần hợp tác làm nét đẹp văn hóa tất yếu trong Học viện.</li></ul>',
        2, 'Introduce', NULL, 'ACTIVE', 'Nguyễn Tiến Đạt', '2021-03-20', 'Nguyễn Tiến Đạt',
        '2021-03-20'),
       (2,
        'https://res.cloudinary.com/dralhpdiv/image/upload/c_scale,h_162,w_260/v1646968828/news/unknow_news.jpg',
        'Tại sao nên chọn ACT ?', 'Tại sao nên chọn ACT ?',
        '<h1>Tại Sao nên chọn học viện công nghệ thông tin ACT?</h1> <br><h5 style=\"color: rgb(7, 7, 115);text-align: justify;\"> Học viện CNTT ACT trực thuộc sự quản lý của Học viện Kỹ thuật Mật mã được sự ủy quyền của nhiều hãng Công nghệ hàng đầu thế giới: Cisco và Microsoft. </h5> <ul> <li>Để được chứng nhận là Đối tác đào tạo ủy quyền của các hãng Công nghệ hàng đầu thế giới này, Học viện CNTT ACT phải đáp ứng được những yêu cầu khắt khe nhất về đội ngũ giảng viên, cơ sở vật chất, trang thiết bị thực hành cũng như quy trình đào tạo và chăm sóc Học viên nhằm đảm bảo chất lượng đào tạo tại ACT hay bất cứ Trung tâm đào tạo ủy quyền nào khác trên toàn thế giới là đồng nhất, đúng theo tiêu chuẩn của chính hãng.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Đội ngũ giảng viên là các Chuyên gia giỏi nhất trong lĩnh vực giảng dạy </h5> <ul> <li>Đội ngũ giảng viên là các Chuyên gia giỏi nhất trong lĩnh vực giảng dạy.</li><li>Để tham gia giảng dạy tại ACT, giảng viên phải trải qua quy trình tuyển dụng và sát hạch khắt khe, đảm bảo về số năm kinh nghiệm thực tế, kiến thức, kỹ năng sư phạm và sở hữu các chứng chỉ chuyên môn cấp cao nhất trong lĩnh vựa giảng dạy.</li><li>Giảng viên đến từ Học viện Kỹ thuật Mật mã, Đại học FPT…; các trường đào tạo về CNTT hàng đầu Việt Nam, do chính hãng đào tạo, sát hạch và được cấp chứng chỉ là Giảng viên ủy quyền của hãng.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Cơ sở vật chất </h5> <ul> <li>Không gian thực hành và hệ thống trang thiết bị thực hành Lab là những yếu tố then chốt mà ACT phải đáp ứng để có được sự ủy quyền từ các hãng CNTT hàng đầu thế giới.</li><li>Hệ thống gồm hàng trăm bộ định tuyến, máy tính cấu hình cao và các thiết bị CNTT hiện.</li><li>Đảm bảo 100% Học viên có đầy đủ trang thiết bị, máy móc, mang lại cho Học viên môi trường học tập tốt nhất, theo đúng tiêu chuẩn của hãng.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Đa dạng hóa các khóa học </h5> <ul> <li>Nhằm giúp Học viên có nhiều lựa chọn, Học viện CNTT ACT luôn có chương trình đào tạo phù hợp với mong muốn và nhu cầu của các Học viên, cho dù bạn muốn tham gia một khóa học cơ bản về Netwwork hay các khóa học chuyên đề mới như Wordpress hay hơn nữa là phát triển toàn diện bản thân như Soft Skills Training…</li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Lịch khai giảng </h5> <ul> <li>Duy trì lịch khai giảng đều đặn đối với tất cả các khóa học.</li><li>Học viện CNTT ACT là đơn vị duy nhất đảm bảo luôn tổ chức các buổi khai giảng và học thử miễn phí cho tất cả các khóa học. Chúng tôi luôn trân trọng sự quan tâm, tin tưởng và đồng hành của tất cả các bạn, vì vậy, chúng tôi luôn dành đến các bạn sự chăm sóc tận tình và chu đáo nhất.</li><li>Học viện CNTT ACT cũng là đơn vị duy nhất theo sát Học viên trong từng giờ học và cam kết hoàn trả học phí 100% nếu Học viên không thu lại kết quả như mong muốn. Gần 10 năm kinh nghiệm trong đào tạo CNTT chuẩn quốc tế.</li><li>Thành lập từ năm 2008 đến nay, Học viện CNTT ACT đã đào tạo gần 2000 Học viên. Chúng tôi không chỉ đào tạo mà còn là cầu nối tương tác, hỗ trợ và giới thiệu việc làm, giúp Học viên hoàn thiện ước mơ trở thành chuyên viên cao cấp trong các tập đoàn CNTT hàng đầu Việt Nam và thế giới, tạo nên một cộng đồng CNTT mạnh tại Việt Nam.</li><li>Trọn gói học và thi chứng chỉ quốc tế</li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Học viện CNTT ACT đang có các chương trình đào tạo, tin chỉ quốc tế của Cisco và Microsoft: </h5> <ul> <li>Chuyên viên mạng quốc tế CCNA, CCNP, CCNA Security.</li><li>Chuyên viên quản trị hệ thống MCSA, MCITP, MCSE.</li><li>Khóa học về CEH Chương trình Linux LPI.</li><li>Các khoá học lập trình cơ bản và chuyên sâu: C, C++ , C#, Java, Python, PHP&MySQL.</li><li>Sau khi học xong Học viên sẽ được tham gia kỳ thi cấp chứng chỉ quốc tế của hãng ủy quyền.</li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Quy trình tư vấn, chăm sóc Học viên và khách hàng </h5> <ul> <li>Tuân thủ theo hệ thống quản lý chất lượng nhằm đảm bảo quyền lợi tối đa của Học viên.</li><li>Học viên được tư vấn hoàn toàn miễn phí về lộ trình học tập và phát triển nghề nghiệp phù hợp với mục tiêu của bản thân.</li><li>Mọi phản hồi của Học viên đều được xử lý trong vòng 24h bởi đội ngũ Chuyên viên tư vấn chất lượng cao.</li><li>Đảm bảo 95% Học viên hài lòng khi đến với Học viện CNTT ACT. </li></ul> <h5 style=\"color: rgb(7, 7, 115);\"> Khách hàng tiêu biểu của Học viện CNTT ACT: </h5> <ul> <li>Công ty cổ phần phát triển phần mềm và hỗ trợ công nghệ MISOFT.</li><li>Cán bộ Ban cơ yếu chính phủ - Trung tâm Công nghệ thông tin và giám sát An ninh mạng.</li><li>Chuyên viên thuộc Công ty An ninh mạng VSECS.</li><li>Chuyên viên thuộc Công ty An ninh mạng Mi68.</li><li>Chuyên viên thuộc Công ty giải pháp Công nghệ thông tin F99.</li><li>Chuyên viên thuộc Tập đoàn Vinaconnect.</li><li>Chuyên viên thuộc công ty giáp pháp mạng Vinasaigon.</li><li>Sinh viên Học viện Kỹ thuật Mật mã.</li><li>Sinh viên Sinh viên Học viện Bưu Chính Viễn thông.</li><li>Sinh viên Đại Học Công nghệ Giao thông Vận tải</li><li>Sinh viên Học viện An ninh Nhân dân.</li><li>Sinh viên Đại học Khoa học tự nhiên.</li><li>Sinh viên Viện đại học Mở.</li></ul>',
        2, 'Introduce', NULL, 'ACTIVE', 'Nguyễn Tiến Đạt', '2021-03-20', 'Nguyễn Tiến Đạt',
        '2021-03-20');