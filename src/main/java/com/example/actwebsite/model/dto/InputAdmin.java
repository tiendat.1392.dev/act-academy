package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

@Data
public class InputAdmin {
  private String img;
  private String name;
  private String sex;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dob;

  private String phone;
  private String email;
  private String password;
  private String role;
  private String address;
  private String job;
  private String workplace;
  private MultipartFile file;
}
