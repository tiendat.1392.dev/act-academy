package com.example.actwebsite.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class InputJob {
  private String img;
  private String password;
  private String name;
  private String desc;
  private String contact;
  private String phone;
  private String email;
  private String address;
  private String content;
  private String type;
  private MultipartFile file;
}
