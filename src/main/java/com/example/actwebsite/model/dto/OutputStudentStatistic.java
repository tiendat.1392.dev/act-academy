package com.example.actwebsite.model.dto;

import lombok.Data;

@Data
public class OutputStudentStatistic {
  private String className;
  private Long totalStudent;
  private Long numberNewStudent = 0L;
  private Long numberWaitStudent = 0L;
  private Long numberPayHalfStudent = 0L;
  private Long numberConfirmStudent = 0L;
}
