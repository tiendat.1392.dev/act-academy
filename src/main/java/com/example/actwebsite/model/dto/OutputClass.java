package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputClass {
  private Integer id;
  private String fullName;
  private String name;
  private String howToLearn;
  private Integer numberStudent;
  private String classStatus;
  private LocalDate dateOpening;
  private String teacherName;
  private OutputAdmin manager;
  private OutputAdmin supporter;
  private OutputCourse course;
  private String special;
  private String state;
  private String createdBy;
  private LocalDate createdDate;
  private String updatedBy;
  private LocalDate updatedDate;
}
