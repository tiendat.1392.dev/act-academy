package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputJob {
  private Integer id;
  private String password;
  private String img;
  private String name;
  private String desc;
  private String contact;
  private String phone;
  private String email;
  private String address;
  private String content;
  private LocalDate datePosted;
  private String type;
}
