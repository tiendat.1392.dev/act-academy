package com.example.actwebsite.model.dto;

import lombok.Data;

@Data
public class InputUsersClassAdmin {
  private Integer classId;
  private Integer collaboratorId;
  private Integer studentId;
}
