package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputUsersClassAdmin {
  private Integer id;
  private OutputClass classCourse;
  private OutputAdmin collaborator;
  private OutputUser student;
  private String state;
  private String createdBy;
  private LocalDate createdDate;
  private String updatedBy;
  private LocalDate updatedDate;
}
