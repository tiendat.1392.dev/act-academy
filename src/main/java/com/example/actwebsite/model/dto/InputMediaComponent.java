package com.example.actwebsite.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class InputMediaComponent {
  private MultipartFile file;
}
