package com.example.actwebsite.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class InputCourse {
  private String img;
  private String name;
  private String desc;
  private String content;
  private Double price;
  private Integer totalHourStudy;
  private Integer managerId;
  private String type;
  private MultipartFile file;
}
