package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputUser {
  private Integer id;
  private String img;
  private String name;
  private String sex;
  private LocalDate dob;
  private String phone;
  private String email;
  private String address;
  private String job;
  private String workplace;
  private Boolean topStudent;
  private String feedback;
  private String state;
  private String createdBy;
  private LocalDate createdDate;
  private String updatedBy;
  private LocalDate updatedDate;
}
