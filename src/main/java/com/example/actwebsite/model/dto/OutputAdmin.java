package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputAdmin {
  private Integer id;
  private String img;
  private String name;
  private String sex;
  private LocalDate dob;
  private String phone;
  private String email;
  private String password;
  private String role;
  private String address;
  private String job;
  private String workplace;
  private String state;
  private String createdBy;
  private LocalDate createdDate;
  private String updatedBy;
  private LocalDate updatedDate;
}
