package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputNews {
  private Integer id;
  private String img;
  private String name;
  private String desc;
  private String text;
  private LocalDate dateCreated;
  private OutputAdmin poster;
  private String type;
  private String source;
  private String state;
  private String createdBy;
  private LocalDate createdDate;
  private String updatedBy;
  private LocalDate updatedDate;
}
