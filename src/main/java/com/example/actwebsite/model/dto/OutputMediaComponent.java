package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputMediaComponent {
  private Integer id;
  private String name;
  private String img;
  private LocalDate dateCreated;
  private Integer posterId;
  private String url;
}
