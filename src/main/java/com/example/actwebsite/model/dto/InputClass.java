package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class InputClass {
  private String fullName;
  private String name;
  private String howToLearn;
  private Integer numberStudent;
  private String classStatus;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dateOpening;

  private String teacherName;
  private Integer managerId;
  private Integer supporterId;
  private Integer courseId;
  private String special;
  private String state;
}
