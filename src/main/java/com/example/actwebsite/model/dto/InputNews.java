package com.example.actwebsite.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class InputNews {
  private String img;
  private String name;
  private String desc;
  private String text;
  private Integer posterId;
  private String type;
  private String source;
  private MultipartFile file;
}
