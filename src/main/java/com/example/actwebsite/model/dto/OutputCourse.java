package com.example.actwebsite.model.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class OutputCourse {
  private Integer id;
  private String img;
  private String name;
  private String desc;
  private String content;
  private Double price;
  private Integer totalHourStudy;
  private Integer managerId;
  private String type;
  private String state;
  private String createdBy;
  private LocalDate createdDate;
  private String updatedBy;
  private LocalDate updatedDate;
}
