package com.example.actwebsite.model.request;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatisticTimeRequest {
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dateStart;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dateEnd;
}
