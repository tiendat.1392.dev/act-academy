package com.example.actwebsite.model.request;

import lombok.Data;

@Data
public class IdRequest {
  int id;
}
