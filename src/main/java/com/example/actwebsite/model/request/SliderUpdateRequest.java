package com.example.actwebsite.model.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class SliderUpdateRequest {
  private MultipartFile file1;
  private MultipartFile file2;
  private MultipartFile file3;
  private MultipartFile file4;
  private MultipartFile file5;
}
