package com.example.actwebsite.model.projection;

public interface CourseStatisticProjection {
  String getType();

  Long getNumberCourse();
}
