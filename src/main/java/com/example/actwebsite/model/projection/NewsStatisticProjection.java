package com.example.actwebsite.model.projection;

public interface NewsStatisticProjection {
  String getState();

  String getType();

  Long getNumberNews();
}
