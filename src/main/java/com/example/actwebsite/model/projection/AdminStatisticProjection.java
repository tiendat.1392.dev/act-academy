package com.example.actwebsite.model.projection;

public interface AdminStatisticProjection {
  String getRole();

  Long getNumberCollaborator();
}
