package com.example.actwebsite.model.projection;

public interface JobStatisticProjection {
  String getType();

  Long getNumberJob();
}
