package com.example.actwebsite.model.projection;

public interface ClassStatisticProjection {
  String getState();

  Long getNumberClass();
}
