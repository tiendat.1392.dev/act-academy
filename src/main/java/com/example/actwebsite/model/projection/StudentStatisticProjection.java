package com.example.actwebsite.model.projection;

import com.example.actwebsite.entity.Class;

public interface StudentStatisticProjection {
  Class getClassCourse();

  String getState();

  Long getNumberStudent();
}
