package com.example.actwebsite.controller.collaborator;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.NewsStateConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.model.dto.OutputStudentStatistic;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.CourseService;
import com.example.actwebsite.service.JobService;
import com.example.actwebsite.service.NewsService;
import com.example.actwebsite.service.UserClassAdminService;
import com.example.actwebsite.service.UserService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/collaborator")
public class CollaboratorHomeController {

  private final JobService jobService;
  private final NewsService newsService;
  private final UserService userService;
  private final AdminService adminService;
  private final ClassService classService;
  private final CourseService courseService;
  private final UserClassAdminService userClassAdminService;

  @Autowired
  public CollaboratorHomeController(
      JobService jobService,
      NewsService newsService,
      UserService userService,
      AdminService adminService,
      ClassService classService,
      CourseService courseService,
      UserClassAdminService userClassAdminService) {
    this.jobService = jobService;
    this.newsService = newsService;
    this.userService = userService;
    this.adminService = adminService;
    this.classService = classService;
    this.courseService = courseService;
    this.userClassAdminService = userClassAdminService;
  }

  /** Lấy trang Home của Admin (CTV, Quản lý) */
  @GetMapping("/home-statistic")
  public String getHomeAdmin(
      Model model,
      @ModelAttribute StatisticTimeRequest statisticTimeRequest,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("statisticTime", new StatisticTimeRequest());
    Map<String, Long> collaborators =
        adminService.countNumberCollaboratorByRole(statisticTimeRequest);
    model.addAttribute("totalCollaborator", collaborators.get("totalCollaborator"));
    model.addAttribute("numberAdmin", collaborators.get(RoleConstant.Manager.name()));
    model.addAttribute("numberCollaborator", collaborators.get(RoleConstant.Collaborator.name()));
    Map<String, Long> courses = courseService.countNumberCourseByType(statisticTimeRequest);
    model.addAttribute("totalCourse", courses.get("totalCourse"));
    model.addAttribute("numberCourseProgramming", courses.get(TypeConstant.Programming.name()));
    model.addAttribute("numberCourseNetwork", courses.get(TypeConstant.Network.name()));
    Map<String, Long> classes = classService.countNumberClassByState(statisticTimeRequest);
    model.addAttribute("totalClass", classes.get("totalClass"));
    model.addAttribute("numberClassPrep", classes.get(ClassStateConstant.PREP.name()));
    model.addAttribute("numberClassWait", classes.get(ClassStateConstant.WAIT.name()));
    model.addAttribute("numberClassDoing", classes.get(ClassStateConstant.DOING.name()));
    model.addAttribute("numberClassFinished", classes.get(ClassStateConstant.FINISHED.name()));
    Map<String, Long> jobs = jobService.countNumberJobByType(statisticTimeRequest);
    model.addAttribute("totalJob", jobs.get("totalJob"));
    model.addAttribute("numberJobProgramming", jobs.get(TypeConstant.Programming.name()));
    model.addAttribute("numberJobNetwork", jobs.get(TypeConstant.Network.name()));
    Map<String, Long> news = newsService.countNumberNewsByStateAndType(statisticTimeRequest);
    model.addAttribute("totalNews", news.get("totalNews"));
    model.addAttribute("totalNewsWait", news.get("totalNewsWait"));
    model.addAttribute(
        "numberNewsWaitAcademy",
        news.get(NewsStateConstant.WAIT.name() + TypeConstant.Academy.name()));
    model.addAttribute(
        "numberNewsWaitTech", news.get(NewsStateConstant.WAIT.name() + TypeConstant.Tech.name()));
    model.addAttribute(
        "numberNewsWaitQA",
        news.get(NewsStateConstant.WAIT.name() + TypeConstant.Introduce.name()));
    model.addAttribute("totalNewsActive", news.get("totalNewsActive"));
    model.addAttribute(
        "numberNewsActiveAcademy",
        news.get(NewsStateConstant.ACTIVE.name() + TypeConstant.Academy.name()));
    model.addAttribute(
        "numberNewsActiveTech",
        news.get(NewsStateConstant.ACTIVE.name() + TypeConstant.Tech.name()));
    model.addAttribute(
        "numberNewsActiveQA",
        news.get(NewsStateConstant.ACTIVE.name() + TypeConstant.Introduce.name()));
    List<OutputStudentStatistic> outputStudentStatistics =
        userClassAdminService.countNumberStudentByState(statisticTimeRequest);
    model.addAttribute("totalStudent", userService.countAllStudent(statisticTimeRequest));
    model.addAttribute("listStatisticStudent", outputStudentStatistics);
    return "AdminHome";
  }
}
