package com.example.actwebsite.controller.collaborator;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.GenderConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.model.dto.InputAdmin;
import com.example.actwebsite.model.dto.OutputAdmin;
import com.example.actwebsite.model.dto.OutputClass;
import com.example.actwebsite.model.request.ChangePasswordRequest;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.ClassService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/collaborator")
public class CollaboratorInfoController {

  private final GenericMapper genericMapper;
  private final AdminService adminService;
  private final ClassService classService;

  @Autowired
  public CollaboratorInfoController(
      GenericMapper genericMapper, AdminService adminService, ClassService classService) {
    this.genericMapper = genericMapper;
    this.adminService = adminService;
    this.classService = classService;
  }

  /** Lấy trang thông tin chi tiết bản thân */
  @GetMapping("/person-info")
  public String getPerson(@CookieValue(name = "admin") int admin) {
    return "redirect:/collaborator/" + admin + "/class/page/1";
  }

  /** Lấy trang thông tin chi tiết quản lý học viện */
  @GetMapping("/admin-info")
  public String getAdmin() {
    OutputAdmin admin = adminService.findByRole(RoleConstant.Manager.name());
    return "redirect:/collaborator/" + admin.getId() + "/class/page/1";
  }

  /** Lấy trang thông tin chi tiết cộng tác viên */
  @GetMapping("/{collaboratorId}/class/page/{pageId}")
  public String getCollaborator(
      Model model,
      @PathVariable(name = "collaboratorId") int collaboratorId,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputAdmin manager = adminService.findByRole(RoleConstant.Manager.name());
    if (Utils.checkRoleAdmin() || collaboratorId == admin || collaboratorId == manager.getId()) {
      OutputAdmin collaborator = adminService.findById(collaboratorId);
      if (collaborator != null) {
        collaborator.setSex(GenderConstant.valueOf(collaborator.getSex()).getValue());
        int size = 5;
        Page<OutputClass> listClasses =
            classService.getListClassPageableByCollaboratorId(collaboratorId, pageId, size);
        if (notification.getText() != null) {
          switch (notification.getText()) {
            case "unauthorized":
              notification.setText("Bạn không có quyền thực thi hành động này");
              break;
            case "update-success":
              notification.setText("Cập nhật thông tin cộng tác viên thành công");
              break;
            case "update-fail":
              notification.setText(
                  "Cập nhật thông tin cộng tác viên thấy bại <br> Hãy chắn chắn cộng tác viên này có trong hệ thống và email không bị trùng với các cộng tác viên khác");
              break;
            case "change-admin-fail":
              notification.setText(
                  "Đổi quản lý thất bại <br> Hãy chắn chắn cộng tác viên này có trong hệ thống");
              break;
            case "admin-not-change":
              notification.setText(
                  "Đổi quản lý thất bại <br> Email quản lý mới trùng với email quản lý cũ");
              break;
            default:
              notification.setText(null);
              break;
          }
        }
        model.addAttribute("admin", adminService.findById(admin));
        model.addAttribute("collaborator", collaborator);
        model.addAttribute("notification", notification);
        if (listClasses == null || listClasses.isEmpty()) {
          model.addAttribute("page", 0);
          model.addAttribute("size", 0);
          model.addAttribute("pageId", 0);
          model.addAttribute("stt", 0);
          model.addAttribute("listClasses", null);
        } else {
          int page = listClasses.getTotalPages();
          listClasses.forEach(i -> i.setState(ClassStateConstant.valueOf(i.getState()).getValue()));
          model.addAttribute("page", page);
          model.addAttribute("size", size);
          model.addAttribute("pageId", pageId);
          model.addAttribute("stt", Utils.getListNumberPage(page));
          model.addAttribute("listClasses", listClasses);
        }
        return "AdminCollaborator";
      } else {
        return "redirect:/manager/collaborator-management/page/1?text=not-exists";
      }
    } else {
      return "redirect:/collaborator/" + admin + "/class/page/1?text=unauthorized";
    }
  }

  /** Lấy trang chỉnh sửa thông tin cộng tác viên */
  @GetMapping("/get-update-collaborator/{id}")
  public String getUpdateCollaborator(
      Model model,
      @PathVariable(name = "id") int collaboratorId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (Utils.checkRoleAdmin() || collaboratorId == admin) {
      OutputAdmin collaborator = adminService.findById(collaboratorId);
      if (collaborator != null) {
        model.addAttribute("admin", adminService.findById(admin));
        model.addAttribute("collaboratorId", collaborator.getId());
        model.addAttribute("collaborator", genericMapper.mapToType(collaborator, InputAdmin.class));
        return "AdminCollaboratorUpdate";
      } else {
        return "redirect:/manager/collaborator-management/page/1?text=not-exists";
      }
    } else {
      return "redirect:/collaborator/" + admin + "/class/page/1?text=unauthorized";
    }
  }

  /** Chỉnh sửa thông tin cộng tác viên */
  @PostMapping("/update-collaborator/{id}")
  public String updateCollaborator(
      @ModelAttribute InputAdmin data,
      @PathVariable(name = "id") int collaboratorId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (adminService.updateAdmin(collaboratorId, data, admin)) {
      return "redirect:/collaborator/" + collaboratorId + "/class/page/1?text=update-success";
    }
    return "redirect:/collaborator/" + collaboratorId + "/class/page/1?text=update-fail";
  }

  /** Lấy trang đổi mật khẩu tài khoản cộng tác viên */
  @GetMapping("/get-change-password/{collaboratorId}")
  public String getChangePassword(
      Model model,
      @PathVariable(name = "collaboratorId") int collaboratorId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (collaboratorId == admin) {
      OutputAdmin collaborator = adminService.findById(collaboratorId);
      if (collaborator != null) {
        if (notification.getText() != null) {
          switch (notification.getText()) {
            case "change-password-fail":
              notification.setText(
                  "Đổi mật khẩu thất bại<br>Hãy kiểm tra mật khẩu cũ bạn nhập vào");
              break;
            case "same-old-password":
              notification.setText("Mật khẩu mới giống với mật khẩu cũ");
              break;
            default:
              notification.setText(null);
              break;
          }
        }
        model.addAttribute("admin", adminService.findById(admin));
        model.addAttribute("collaboratorId", collaboratorId);
        model.addAttribute("request", new ChangePasswordRequest());
        model.addAttribute("notification", notification);
        return "AdminChangePassword";
      } else {
        return "redirect:/home-statistic";
      }
    } else {
      return "redirect:/collaborator/" + admin + "/class/page/1?text=unauthorized";
    }
  }

  /** Đổi mật khẩu tài khoản cộng tác viên */
  @PostMapping("/change-password/{id}")
  public String changePassword(
      HttpServletRequest request,
      HttpServletResponse response,
      @PathVariable(name = "id") int id,
      @ModelAttribute ChangePasswordRequest data) {
    if (data.getOldPassword().equals(data.getNewPassword())) {
      return "redirect:/collaborator/get-change-password/" + id + "?text=same-old-password";
    } else {
      if (adminService.changePassword(id, data)) {
        Utils.refreshCookie(response, request);
        return "redirect:/login-admin?text=change-password-success";
      } else {
        return "redirect:/collaborator/get-change-password/" + id + "?text=change-password-fail";
      }
    }
  }
}
