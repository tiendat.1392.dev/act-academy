package com.example.actwebsite.controller.collaborator;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.GenderConstant;
import com.example.actwebsite.common.enums.StudentStateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.model.dto.OutputClass;
import com.example.actwebsite.model.dto.OutputUsersClassAdmin;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.UserClassAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/collaborator")
public class CollaboratorClassController {

  private final UserClassAdminService userClassAdminService;
  private final AdminService adminService;
  private final ClassService classService;

  @Autowired
  public CollaboratorClassController(
      UserClassAdminService userClassAdminService,
      AdminService adminService,
      ClassService classService) {
    this.userClassAdminService = userClassAdminService;
    this.adminService = adminService;
    this.classService = classService;
  }

  /** Lấy trang quản lý lớp học */
  @GetMapping("/class-management/page/{pageId}")
  public String getClassManagement(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (search == null) {
      search = new SearchRequest();
    }
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "not-exists":
          notification.setText("Lớp học không tồn tại");
          break;
        case "create-success":
          notification.setText("Tạo mới lớp học thành công");
          break;
        case "create-fail":
          notification.setText(
              "Tạo mới lớp học thất bại <br> Hãy chắc chắn tên lớp học không bị trùng với các lớp học khác");
          break;
        case "update-success":
          notification.setText("Cập nhật thông tin lớp học thành công");
          break;
        case "update-fail":
          notification.setText(
              "Cập nhật thông tin lớp học thất bại <br> Hãy chắc chắn lớp học này có tồn tại và tên không bị trùng với các lớp học khác");
          break;
        case "delete-success":
          notification.setText("Xóa lớp học thành công");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    int size = 5;
    Page<OutputClass> listClasses = classService.getListClassPageable(search, pageId, size);
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("notification", notification);
    model.addAttribute("search", search);
    if (listClasses == null || listClasses.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listClasses", null);
    } else {
      int page = listClasses.getTotalPages();
      listClasses.forEach(i -> i.setState(ClassStateConstant.valueOf(i.getState()).getValue()));
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listClasses", listClasses);
    }
    return "AdminClassManagement";
  }

  /** Lấy trang thông tin chi tiết lớp học */
  @GetMapping("/class/{id}/student/page/{pageId}")
  public String getClass(
      Model model,
      @PathVariable(name = "id") int id,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute SearchRequest search,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputClass outputClass = classService.findById(id);
    if (outputClass != null) {
      int size = 5;
      outputClass.setState(ClassStateConstant.valueOf(outputClass.getState()).getValue());
      Page<OutputUsersClassAdmin> listStudents =
          userClassAdminService.findByClassId(outputClass.getId(), search, pageId, size);
      if (search == null) {
        search = new SearchRequest();
      }
      if (notification.getText() != null) {
        switch (notification.getText()) {
          case "create-student-success":
            notification.setText("Thêm học viên vào lớp học thành công");
            break;
          case "create-student-fail":
            notification.setText(
                "Thêm học viên vào lớp học thất bại<br> Hãy kiếm tra lại học viên đã có trong lớp học hoặc trong hệ thống chưa bằng email");
            break;
          case "change-student-state-success":
            notification.setText("Thay đổi trạng thái học viên trong lớp học thành công");
            break;
          case "change-student-state-fail":
            notification.setText(
                "Thay đổi trạng thái học viên trong lớp học thất bại <br> Hãy chắc chắn học viên này có trong lớp học");
            break;
          case "delete-student-success":
            notification.setText("Xóa học viên trong lớp học thành công");
            break;
          case "delete-student-fail":
            notification.setText(
                "Xóa học viên trong lớp học thất bại <br> Hãy chắc chắn lớp học có tồn tại và học viên này có trong lớp");
            break;
          default:
            notification.setText(null);
            break;
        }
      }
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("notification", notification);
      model.addAttribute("class", outputClass);
      model.addAttribute("search", search);
      if (listStudents == null || listStudents.isEmpty()) {
        model.addAttribute("page", 0);
        model.addAttribute("size", 0);
        model.addAttribute("pageId", 0);
        model.addAttribute("stt", 0);
        model.addAttribute("listStudents", null);
      } else {
        int page = listStudents.getTotalPages();
        listStudents.forEach(
            i -> {
              i.getStudent().setSex(GenderConstant.valueOf(i.getStudent().getSex()).getValue());
              i.setState(StudentStateConstant.valueOf(i.getState()).getValue());
            });
        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("pageId", pageId);
        model.addAttribute("stt", Utils.getListNumberPage(page));
        model.addAttribute("listStudents", listStudents);
      }
      return "AdminClass";
    } else {
      return "redirect:/collaborator/class-management/page/1?text=not-exists";
    }
  }

  /** Thêm học viên vào lớp học */
  @GetMapping("/class/{classId}/add-student/{email}")
  public String addStudent(
      @PathVariable(name = "classId") int classId,
      @PathVariable(name = "email") String email,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (userClassAdminService.addStudentToClassByEmail(classId, email, admin)) {
      return "redirect:/collaborator/class/"
          + classId
          + "/student/page/1?text=create-student-success";
    } else {
      return "redirect:/collaborator/class/" + classId + "/student/page/1?text=create-student-fail";
    }
  }
}
