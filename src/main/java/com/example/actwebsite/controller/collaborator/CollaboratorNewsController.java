package com.example.actwebsite.controller.collaborator;

import com.example.actwebsite.common.enums.NewsStateConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.model.dto.InputNews;
import com.example.actwebsite.model.dto.OutputNews;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.NewsService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("collaborator")
public class CollaboratorNewsController {

  private final GenericMapper genericMapper;
  private final AdminService adminService;
  private final NewsService newsService;

  @Autowired
  public CollaboratorNewsController(
      GenericMapper genericMapper, AdminService adminService, NewsService newsService) {
    this.genericMapper = genericMapper;
    this.adminService = adminService;
    this.newsService = newsService;
  }

  /** Lấy trang quản lý tin tức */
  @GetMapping("/news-management/page/{pageId}")
  public String getNewsManagement(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (search == null) {
      search = new SearchRequest();
    }
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "unauthorized":
          notification.setText("Bạn không có quyền thực thi hành động này");
          break;
        case "not-exists":
          notification.setText("Tin tức không tồn tại");
          break;
        case "create-success":
          notification.setText("Tạo tin tức mới thành công");
          break;
        case "create-fail":
          notification.setText(
              "Tạo tin tức mới thất bại <br> Hãy chắc chắn tiêu đề tin tức không bị trùng với các tin tức khác");
          break;
        case "update-success":
          notification.setText("Chỉnh sửa tin tức thành công");
          break;
        case "update-fail":
          notification.setText(
              "Chỉnh sửa tức thất bại <br> Hãy chắc chắn tin tức có tồn tại và tiêu đề tin tức không bị trùng với các tin tức khác");
          break;
        case "delete-success":
          notification.setText("Xóa tin tức thành công");
          break;
        case "accept-news":
          notification.setText("Duyệt tin tức thành công");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    int size = 5;
    Page<OutputNews> listNews = newsService.getListNewsPageable(search, null, pageId, size);
    model.addAttribute("search", search);
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("notification", notification);
    if (listNews == null || listNews.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listNews", null);
    } else {
      int page = listNews.getTotalPages();
      listNews.forEach(
          i -> {
            i.setType(TypeConstant.valueOf(i.getType()).getValue());
            i.setState(NewsStateConstant.valueOf(i.getState()).getValue());
          });
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listNews", listNews);
    }
    return "AdminNewsManagement";
  }

  /** Lấy trang thông tin chi tiết tin tức */
  @GetMapping("/news/{id}")
  public String getNews(
      Model model,
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputNews news = newsService.findById(id);
    if (news != null) {
      news.setType(TypeConstant.valueOf(news.getType()).getValue());
      news.setState(NewsStateConstant.valueOf(news.getState()).getValue());
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("news", news);
      return "AdminNews";
    } else {
      return "redirect:/collaborator/news-management/page/1?text=not-exists";
    }
  }

  /** Lấy trang thêm tin tức */
  @GetMapping("/get-create-news")
  public String getCreateNews(
      Model model, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("news", new InputNews());
    return "AdminNewsCreate";
  }

  /** Lấy trang chỉnh sửa tin tức */
  @GetMapping("/get-update-news/{id}")
  public String getUpdateNews(
      Model model,
      HttpServletRequest request,
      @PathVariable(name = "id") int newsId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputNews news = newsService.findById(newsId);
    if (news != null) {
      if (Utils.checkRoleAdmin() || news.getPoster().getId() == admin) {
        model.addAttribute("admin", adminService.findById(admin));
        model.addAttribute("newsId", news.getId());
        model.addAttribute("news", genericMapper.mapToType(news, InputNews.class));
        return "AdminNewsUpdate";
      } else {
        return "redirect:/collaborator/news-management/page/1?text=unauthorized";
      }
    } else {
      return "redirect:/collaborator/news-management/page/1?text=not-exists";
    }
  }

  /** Thêm tin tức */
  @PostMapping("/create-news")
  public String createNews(
      @ModelAttribute InputNews data, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (newsService.createNews(data, admin)) {
      return "redirect:/collaborator/news-management/page/1?text=create-success";
    }
    return "redirect:/collaborator/news-management/page/1?text=create-fail";
  }

  /** Chính sửa tin tức */
  @PostMapping("/update-news/{id}")
  public String updateNews(
      @ModelAttribute InputNews data,
      @PathVariable(name = "id") int newsId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (newsService.updateNews(newsId, data, admin)) {
      return "redirect:/collaborator/news-management/page/1?text=update-success";
    }
    return "redirect:/collaborator/news-management/page/1?text=update-fail";
  }

  /** Xóa tin tức */
  @GetMapping("/delete-news/{id}")
  public String deleteNews(
      @PathVariable(name = "id") int newsId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputNews news = newsService.findById(newsId);
    if (news != null) {
      if (Utils.checkRoleAdmin() || news.getPoster().getId() == admin) {
        newsService.changeStateNews(newsId, NewsStateConstant.DELETED.name(), admin);
        return "redirect:/collaborator/news-management/page/1?text=delete-success";
      } else {
        return "redirect:/collaborator/news-management/page/1?text=unauthorized";
      }
    } else {
      return "redirect:/collaborator/news-management/page/1?text=not-exists";
    }
  }
}
