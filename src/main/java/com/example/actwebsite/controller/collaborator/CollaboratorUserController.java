package com.example.actwebsite.controller.collaborator;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.GenderConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.model.dto.InputUser;
import com.example.actwebsite.model.dto.OutputAdmin;
import com.example.actwebsite.model.dto.OutputUser;
import com.example.actwebsite.model.dto.OutputUsersClassAdmin;
import com.example.actwebsite.model.request.IdRequest;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.UserClassAdminService;
import com.example.actwebsite.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/collaborator")
public class CollaboratorUserController {

  private final UserClassAdminService userClassAdminService;
  private final GenericMapper genericMapper;
  private final ClassService classService;
  private final AdminService adminService;
  private final UserService userService;

  @Autowired
  public CollaboratorUserController(
      UserClassAdminService userClassAdminService,
      GenericMapper genericMapper,
      ClassService classService,
      AdminService adminService,
      UserService userService) {
    this.userClassAdminService = userClassAdminService;
    this.genericMapper = genericMapper;
    this.classService = classService;
    this.adminService = adminService;
    this.userService = userService;
  }

  /** Trang quản lý học viên */
  @GetMapping("/user-management/page/{pageId}")
  public String getUserManagement(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    int size = 5;
    if (search == null) {
      search = new SearchRequest();
    }
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "unauthorized":
          notification.setText("Bạn không có quyền thực thi hành động này");
          break;
        case "not-exists":
          notification.setText("Học viên không tồn tại");
          break;
        case "create-success":
          notification.setText("Thêm học viên thành công");
          break;
        case "create-fail":
          notification.setText(
              "Thêm học viên thất bại <br> Hãy chắc chắn học viên này chưa có thông tin trong lớp bạn chọn");
          break;
        case "update-success":
          notification.setText("Cập nhật thông tin học viên thành công");
          break;
        case "update-fail":
          notification.setText(
              "Cập nhật thông tin học viên thất bại <br> Hãy chắc chắn học viên này có tồn tại và không bị trùng email với các học viên khác");
          break;
        case "delete-success":
          notification.setText("Xóa học viên thành công");
          break;
        case "set-top-student-success":
          notification.setText("Đặt làm Học viên tiêu biểu thành công");
          break;
        case "set-top-student-fail":
          notification.setText(
              "Đặt làm Học viên tiêu biểu thất bại <br> Hãy chắc chắn học viên đã có thông tin trong hệ thống và số lượng Học viên tiêu biểu chưa vượt quá 20");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    Page<OutputUser> listUsers = userService.getListUserPageable(search, pageId, size);
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("notification", notification);
    model.addAttribute("search", search);
    if (listUsers == null || listUsers.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listUsers", null);
    } else {
      int page = listUsers.getTotalPages();
      listUsers.forEach(i -> i.setSex(GenderConstant.valueOf(i.getSex()).getValue()));
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listUsers", listUsers);
    }
    return "AdminUserManagement";
  }

  /** Lấy trang thông tin chi tiết học viên */
  @GetMapping("/user/{id}/class/page/{pageId}")
  public String getUser(
      Model model,
      @PathVariable(name = "id") int id,
      @PathVariable(name = "pageId") int pageId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputUser student = userService.findById(id);
    if (student != null) {
      student.setSex(GenderConstant.valueOf(student.getSex()).getValue());
      int size = 5;
      Page<OutputUsersClassAdmin> listClasses =
          userClassAdminService.findByStudentId(id, pageId, size);
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("student", student);
      if (listClasses == null || listClasses.isEmpty()) {
        model.addAttribute("page", 0);
        model.addAttribute("size", 0);
        model.addAttribute("pageId", 0);
        model.addAttribute("stt", 0);
        model.addAttribute("listClasses", null);
      } else {
        int page = listClasses.getTotalPages();
        listClasses.forEach(
            i -> {
              String value = ClassStateConstant.valueOf(i.getClassCourse().getState()).getValue();
              i.getClassCourse().setState(value);
            });
        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("pageId", pageId);
        model.addAttribute("stt", Utils.getListNumberPage(page));
        model.addAttribute("listClasses", listClasses);
      }
      return "AdminUser";
    } else {
      return "redirect:/collaborator/user-management/page/1?text=not-exists";
    }
  }

  /** Lấy trang thêm thông tin học viên */
  @GetMapping("/get-create-user")
  public String getCreateUser(
      Model model, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("listClass", classService.findAll());
    model.addAttribute("student", new InputUser());
    model.addAttribute("classId", new IdRequest());
    return "AdminUserCreate";
  }

  /** Lấy trang chỉnh sửa thông tin học viên */
  @GetMapping("/get-update-user/{id}")
  public String getUpdateUser(
      Model model,
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputUser student = userService.findById(id);
    OutputAdmin collaborator = adminService.findById(admin);
    if (student != null) {
      if (Utils.checkRoleAdmin() || student.getCreatedBy().equals(collaborator.getName())) {
        model.addAttribute("admin", collaborator);
        model.addAttribute("studentId", student.getId());
        model.addAttribute("student", genericMapper.mapToType(student, InputUser.class));
        return "AdminUserUpdate";
      } else {
        return "redirect:/collaborator/user-management/page/1?text=unauthorized";
      }
    } else {
      return "redirect:/collaborator/user-management/page/1?text=not-exists";
    }
  }

  /** Thêm thông tin học viên */
  @PostMapping("/create-user")
  public String createUser(
      @ModelAttribute InputUser data,
      @ModelAttribute IdRequest classId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (userClassAdminService.createStudentWithClass(data, classId.getId(), admin)) {
      return "redirect:/collaborator/user-management/page/1?text=create-success";
    } else {
      return "redirect:/collaborator/user-management/page/1?text=create-fail";
    }
  }

  /** Chỉnh sửa thông tin học viên */
  @PostMapping("/update-user/{id}")
  public String updateUser(
      @ModelAttribute InputUser data,
      @ModelAttribute IdRequest classId,
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (userService.updateUser(id, data, admin)) {
      return "redirect:/collaborator/user-management/page/1?text=update-success";
    }
    return "redirect:/collaborator/user-management/page/1?text=update-fail";
  }
}
