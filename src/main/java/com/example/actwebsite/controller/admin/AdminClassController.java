package com.example.actwebsite.controller.admin;

import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.model.dto.InputClass;
import com.example.actwebsite.model.dto.OutputClass;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.CourseService;
import com.example.actwebsite.service.UserClassAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminClassController {

  private final UserClassAdminService userClassAdminService;
  private final GenericMapper genericMapper;
  private final AdminService adminService;
  private final ClassService classService;
  private final CourseService courseService;

  @Autowired
  public AdminClassController(
      AdminService adminService,
      ClassService classService,
      CourseService courseService,
      UserClassAdminService userClassAdminService,
      GenericMapper genericMapper) {
    this.adminService = adminService;
    this.classService = classService;
    this.courseService = courseService;
    this.userClassAdminService = userClassAdminService;
    this.genericMapper = genericMapper;
  }

  /** Lấy trang thêm lớp học */
  @GetMapping("/get-create-class")
  public String getCreateClass(
      Model model, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("listCollaborator", adminService.findAll());
    model.addAttribute("listCourse", courseService.findAll());
    model.addAttribute("class", new InputClass());
    return "AdminClassCreate";
  }

  /** Lấy trang chỉnh sửa thông tin lớp học */
  @GetMapping("/get-update-class/{id}")
  public String getUpdateClass(
      Model model,
      @CookieValue(name = "admin", defaultValue = "0") int admin,
      @PathVariable(name = "id") int classId) {
    OutputClass classCourse = classService.findById(classId);
    if (classCourse != null) {
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("listCollaborator", adminService.findAll());
      model.addAttribute("listCourse", courseService.findAll());
      model.addAttribute("classId", classId);
      model.addAttribute("class", genericMapper.mapToType(classCourse, InputClass.class));
      return "AdminClassUpdate";
    } else {
      return "redirect:/collaborator/class-management/page/1";
    }
  }

  /** Thêm lớp học */
  @PostMapping("/create-class")
  public String createClass(
      @ModelAttribute InputClass data, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (classService.createClass(data, admin)) {
      return "redirect:/collaborator/class-management/page/1?text=create-success";
    }
    return "redirect:/collaborator/class-management/page/1?text=create-fail";
  }

  /** Chỉnh sửa thông tin lớp học */
  @PostMapping("/update-class/{id}")
  public String updateClass(
      @ModelAttribute InputClass data,
      @PathVariable(name = "id") int classId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (classService.updateClass(classId, data, admin)) {
      return "redirect:/collaborator/class-management/page/1?text=update-success";
    }
    return "redirect:/collaborator/class-management/page/1?text=update-fail";
  }

  /** Xóa lớp học */
  @GetMapping("/delete-class/{id}")
  public String deleteClass(
      @PathVariable(name = "id") int classId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (classService.deleteClass(classId, admin)) {
      return "redirect:/collaborator/class-management/page/1?text=delete-success";
    }
    return "redirect:/collaborator/class-management/page/1?text=not-exists";
  }

  /** Thay đổi trạng thái học viên trong lớp học */
  @GetMapping("/class/{classId}/student/{studentId}/change-state/{state}")
  public String changeStateStudent(
      @PathVariable(name = "classId") int classId,
      @PathVariable(name = "studentId") int studentId,
      @PathVariable(name = "state") String state,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (userClassAdminService.changeStateStudentInClass(classId, studentId, state, admin)) {
      return "redirect:/collaborator/class/"
          + classId
          + "/student/page/1?text=change-student-state-success";
    } else {
      return "redirect:/collaborator/class/"
          + classId
          + "/student/page/1?text=change-student-state-fail";
    }
  }

  /** Xóa học viên khỏi lớp học */
  @GetMapping("/class/{classId}/delete-student/{studentId}")
  public String deleteStudent(
      @PathVariable(name = "classId") int classId,
      @PathVariable(name = "studentId") int studentId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (userClassAdminService.deleteStudent(classId, studentId, admin)) {
      return "redirect:/collaborator/class/"
          + classId
          + "/student/page/1?text=delete-student-success";
    } else {
      return "redirect:/collaborator/class/" + classId + "/student/page/1?text=delete-student-fail";
    }
  }
}
