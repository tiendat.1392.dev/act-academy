package com.example.actwebsite.controller.admin;

import com.example.actwebsite.model.dto.InputMediaComponent;
import com.example.actwebsite.model.request.SliderUpdateRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.MediaComponentService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminMediaComponentController {

  private final MediaComponentService mediaComponentService;
  private final AdminService adminService;

  @Autowired
  public AdminMediaComponentController(
      MediaComponentService mediaComponentService, AdminService adminService) {
    this.mediaComponentService = mediaComponentService;
    this.adminService = adminService;
  }

  /** Lấy trang thành phần media của website */
  @GetMapping("/media-component")
  public String getMediaComponent(
      Model model, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    Map<String, String> slider = mediaComponentService.getSlider();
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    model.addAttribute("video_update", new InputMediaComponent());
    model.addAttribute("slider1", slider.get("slider_01"));
    model.addAttribute("slider2", slider.get("slider_02"));
    model.addAttribute("slider3", slider.get("slider_03"));
    model.addAttribute("slider4", slider.get("slider_04"));
    model.addAttribute("slider5", slider.get("slider_05"));
    model.addAttribute("slider_update", new SliderUpdateRequest());
    return "AdminMediaComponent";
  }

  /** Sửa slider */
  @PostMapping("/update-slider")
  public String updateSlider(@ModelAttribute SliderUpdateRequest data) {
    mediaComponentService.updateSlider(data);
    return "redirect:/manager/media-component";
  }

  /** Sửa video banner */
  @PostMapping("/update-video-banner")
  public String updateVideoBanner(@ModelAttribute InputMediaComponent data) {
    mediaComponentService.updateVideoBanner(data);
    return "redirect:/manager/media-component";
  }
}
