package com.example.actwebsite.controller.admin;

import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.model.dto.OutputJob;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.JobService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminJobController {

  private final JobService jobService;
  private final AdminService adminService;

  public AdminJobController(JobService jobService, AdminService adminService) {
    this.jobService = jobService;
    this.adminService = adminService;
  }

  /** Lấy trang quản lý tuyển dụng */
  @GetMapping("/job-management/page/{pageId}")
  public String getJobManagement(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (search == null) {
      search = new SearchRequest();
    }
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "not-exists":
          notification.setText("Tin tuyển dụng không tồn tại");
          break;
        case "delete-success":
          notification.setText(
              "Xóa tin tuyển dụng thành công <br> Lý do bạn xóa đã được gửi về email của người đăng");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    int size = 5;
    Page<OutputJob> listJobs = jobService.getListJobPageable(search, pageId, size);
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("notification", notification);
    model.addAttribute("search", search);
    if (listJobs == null || listJobs.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listJobs", null);
    } else {
      int page = listJobs.getTotalPages();
      listJobs.forEach(i -> i.setType(TypeConstant.valueOf(i.getType()).getValue()));
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listJobs", listJobs);
    }
    return "AdminJobManagement";
  }

  /** Lấy trang thông tin chi tiết tuyển dụng */
  @GetMapping("/job/{id}")
  public String getCourse(
      Model model,
      @PathVariable(name = "id") int jobId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputJob job = jobService.findById(jobId);
    if (job != null) {
      job.setType(TypeConstant.valueOf(job.getType()).getValue());
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("job", job);
      return "AdminJob";
    } else {
      return "redirect:/manager/job-management/page/1?text=not-exists";
    }
  }

  /** Xóa tin tuyển dụng */
  @GetMapping("/delete-job/{id}")
  public String deleteJob(
      @PathVariable(name = "id") int jobId, @ModelAttribute(name = "reason") String reason) {
    if (jobService.deleteJob(jobId, reason)) {
      return "redirect:/manager/job-management/page/1?text=delete-success";
    }
    return "redirect:/manager/job-management/page/1?text=not-exists";
  }
}
