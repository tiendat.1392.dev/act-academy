package com.example.actwebsite.controller.admin;

import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.model.dto.InputCourse;
import com.example.actwebsite.model.dto.OutputCourse;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminCourseController {

  private final AdminService adminService;
  private final CourseService courseService;
  private final GenericMapper genericMapper;

  @Autowired
  public AdminCourseController(
      AdminService adminService, CourseService courseService, GenericMapper genericMapper) {
    this.adminService = adminService;
    this.courseService = courseService;
    this.genericMapper = genericMapper;
  }

  /** Lấy trang quản lý khóa học */
  @GetMapping("/course-management/page/{pageId}")
  public String getCourseManagement(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (search == null) {
      search = new SearchRequest();
    }
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "not-exists":
          notification.setText("Khóa học không tồn tại");
          break;
        case "create-success":
          notification.setText("Tạo khóa học mới thành công");
          break;
        case "create-fail":
          notification.setText(
              "Tạo khóa học mới thất bại <br> Hãy chắc chắn tên khóa học không bị trùng với các khóa học khác");
          break;
        case "update-success":
          notification.setText("Cập nhật thông tin khóa học thành công");
          break;
        case "update-fail":
          notification.setText(
              "Cập nhật thông tin khóa học thất bại <br> Hãy chắc chắn khóa học có tồn tại và tên khóa học không bị trùng với các khóa học khác");
          break;
        case "delete-success":
          notification.setText("Xóa khóa học thành công");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    int size = 5;
    Page<OutputCourse> listCourses = courseService.getListCoursePageable(search, pageId, size);
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("notification", notification);
    model.addAttribute("search", search);
    if (listCourses == null || listCourses.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listCourses", null);
    } else {
      int page = listCourses.getTotalPages();
      listCourses.forEach(i -> i.setType(TypeConstant.valueOf(i.getType()).getValue()));
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listCourses", listCourses);
    }
    return "AdminCourseManagement";
  }

  /** Lấy trang thông tin chi tiết khóa học */
  @GetMapping("/course/{id}")
  public String getCourse(
      Model model,
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputCourse course = courseService.findById(id);
    if (course != null) {
      course.setType(TypeConstant.valueOf(course.getType()).getValue());
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("course", course);
      return "AdminCourse";
    } else {
      return "redirect:/manager/course-management/page/1?text=not-exists";
    }
  }

  /** Lấy trang thêm khóa học */
  @GetMapping("/get-create-course")
  public String getCreateCourse(
      Model model, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("course", new InputCourse());
    return "AdminCourseCreate";
  }

  /** Lấy trang chính sửa thông tin khóa học */
  @GetMapping("/get-update-course/{id}")
  public String getUpdateCourse(
      Model model,
      @PathVariable(name = "id") int courseId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputCourse course = courseService.findById(courseId);
    if (course != null) {
      model.addAttribute("admin", adminService.findById(admin));
      model.addAttribute("course", genericMapper.mapToType(course, InputCourse.class));
      model.addAttribute("courseId", courseId);
      return "AdminCourseUpdate";
    } else {
      return "redirect:/manager/course-management/page/1?text=not-exists";
    }
  }

  /** Thêm khóa học */
  @PostMapping("/create-course")
  public String createCourse(
      @ModelAttribute InputCourse course,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (courseService.createCourse(course, admin)) {
      return "redirect:/manager/course-management/page/1?text=create-success";
    }
    return "redirect:/manager/course-management/page/1?text=create-fail";
  }

  /** Chỉnh sửa thông tin khóa học */
  @PostMapping("/update-course/{id}")
  public String updateCourse(
      @ModelAttribute InputCourse data,
      @PathVariable(name = "id") int courseId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (courseService.updateCourse(courseId, data, admin)) {
      return "redirect:/manager/course-management/page/1?text=update-success";
    }
    return "redirect:/manager/course-management/page/1?text=update-fail";
  }

  /** Xóa khóa học */
  @GetMapping("/delete-course/{id}")
  public String deleteCourse(
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (courseService.deleteCourse(id, admin)) {
      return "redirect:/manager/course-management/page/1?text=delete-success";
    }
    return "redirect:/manager/course-management/page/1?text=not-exists";
  }
}
