package com.example.actwebsite.controller.admin;

import com.example.actwebsite.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminUserController {

  private final UserService userService;

  @Autowired
  public AdminUserController(UserService userService) {
    this.userService = userService;
  }

  /** Xóa thông tin học viên */
  @GetMapping("/delete-user/{id}")
  public String deleteUser(
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (userService.deleteUser(id, admin)) {
      return "redirect:/collaborator/user-management/page/1?text=delete-success";
    }
    return "redirect:/collaborator/user-management/page/1?text=not-exists";
  }
}
