package com.example.actwebsite.controller.admin;

import com.example.actwebsite.model.request.LoginRequest;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class AdminAuthenticationController {

  private final AdminService adminService;

  @Autowired
  public AdminAuthenticationController(AdminService adminService) {
    this.adminService = adminService;
  }

  /** Đăng nhập tài khoản Collaborator hoặc Manager */
  @GetMapping("/login-admin")
  public String getLogin(
      Model model,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "login-fail":
          notification.setText(
              "Đăng nhập thất bại !<br>Hãy kiểm tra lại email và mật khẩu của bạn nhé");
          break;
        case "change-password-success":
          notification.setText(
              "Đổi mật khẩu thành công !<br>Hãy đăng nhập lại vào hệ thống với mật khẩu mới");
          break;
        case "forget-password-success":
          notification.setText(
              "Yêu cầu lấy lại mật khẩu của bạn đã được gửi cho quản lý !<br>Xin hãy liên lạc với quản lý để được lấy mật khẩu mới");
          break;
        case "forget-password-fail":
          notification.setText(
              "Yêu cầu lấy lại mật khẩu thất bại !<br>Hãy chắc chắn tài khoản của bạn có trong hệ thống hoặc liên lạc với quản lý để được hỗ trợ");
          break;
        case "change-admin-success":
          notification.setText(
              "Đổi quản lý thành công !<br>Bạn phải đăng nhập lại để tiếp tục sử dụng hệ thống");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    model.addAttribute("loginInfo", new LoginRequest());
    model.addAttribute("notification", notification);
    return "AdminLogin";
  }

  /** Quên mật khẩu */
  @GetMapping("/forget-password/{email}")
  public String forgetPassword(Model model, @PathVariable(name = "email") String email) {
    if (adminService.forgetPassword(email)) {
      return "redirect:/login-admin?text=forget-password-success";
    } else {
      return "redirect:/login-admin?text=forget-password-fail";
    }
  }
}
