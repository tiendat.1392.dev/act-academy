package com.example.actwebsite.controller.admin;

import com.example.actwebsite.common.enums.GenderConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.model.dto.InputAdmin;
import com.example.actwebsite.model.dto.OutputAdmin;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.ClassService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminCollaboratorController {

  public final ClassService classService;
  private final AdminService adminService;

  @Autowired
  public AdminCollaboratorController(AdminService adminService, ClassService classService) {
    this.adminService = adminService;
    this.classService = classService;
  }

  /** Lấy trang quản lý cộng tác viên */
  @GetMapping("/collaborator-management/page/{pageId}")
  public String getCollaboratorManagement(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (search == null) {
      search = new SearchRequest();
    }
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "unauthorized":
          notification.setText("Bạn không có quyền thực thi hành động này");
          break;
        case "not-exists":
          notification.setText("Cộng tác viên không tồn tại");
          break;
        case "create-success":
          notification.setText("Tạo cộng tác viên mới thành công");
          break;
        case "create-fail":
          notification.setText(
              "Tạo cộng tác viên mới thất bại <br> Hãy chắc chắn email của cộng tác viên mới chưa có trong hệ thống");
          break;
        case "delete-success":
          notification.setText("Xóa cộng tác viên thành công");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    int size = 5;
    Page<OutputAdmin> listAdmins = adminService.getListAdminPageable(search, pageId, size);
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("notification", notification);
    model.addAttribute("search", search);
    if (listAdmins == null || listAdmins.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listAdmins", null);
    } else {
      int page = listAdmins.getTotalPages();
      listAdmins.forEach(
          i -> {
            i.setSex(GenderConstant.valueOf(i.getSex()).getValue());
            i.setRole(RoleConstant.valueOf(i.getRole()).getValue());
          });
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listAdmins", listAdmins);
    }
    return "AdminCollaboratorManagement";
  }

  /** Lấy trang thông tin chi tiết quản lý học viện */
  @GetMapping("/admin-info")
  public String getAdmin() {
    OutputAdmin admin = adminService.findByRole(RoleConstant.Manager.name());
    return "redirect:/collaborator/" + admin.getId() + "/class/page/1";
  }

  /** Đổi quản lý học viện */
  @GetMapping("/change-admin/{email}")
  public String changeAdmin(
      HttpServletRequest request,
      HttpServletResponse response,
      @PathVariable(name = "email") String email,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    OutputAdmin manager = adminService.findByRole(RoleConstant.Manager.name());
    if (manager.getEmail().equals(email)) {
      return "redirect:/collaborator/" + manager.getId() + "/class/page/1?text=admin-not-change";
    } else {
      if (adminService.changeAdmin(email, admin)) {
        Utils.refreshCookie(response, request);
        return "redirect:/login-admin?text=change-admin-success";
      } else {
        return "redirect:/collaborator/" + manager.getId() + "/class/page/1?text=change-admin-fail";
      }
    }
  }

  /** Lấy trang thêm thông tin cộng tác viên */
  @GetMapping("/get-create-collaborator")
  public String getCreateCollaborator(
      Model model, @CookieValue(name = "admin", defaultValue = "0") int admin) {
    model.addAttribute("admin", adminService.findById(admin));
    model.addAttribute("collaborator", new InputAdmin());
    return "AdminCollaboratorCreate";
  }

  /** Thêm thông tin cộng tác viên */
  @PostMapping("/create-collaborator")
  public String createCollaborator(
      @CookieValue(name = "admin", defaultValue = "0") int admin, @ModelAttribute InputAdmin data) {
    if (adminService.createAdmin(data, admin)) {
      return "redirect:/manager/collaborator-management/page/1?text=create-success";
    }
    return "redirect:/manager/collaborator-management/page/1?text=create-fail";
  }

  /** Xóa thông tin cộng tác viên */
  @GetMapping("/delete-collaborator/{id}")
  public String deleteCollaborator(
      @PathVariable(name = "id") int id,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (adminService.deleteAdmin(id, admin)) {
      return "redirect:/manager/collaborator-management/page/1?text=delete-success";
    }
    return "redirect:/manager/collaborator-management/page/1?text=not-exists";
  }
}
