package com.example.actwebsite.controller.admin;

import com.example.actwebsite.common.enums.NewsStateConstant;
import com.example.actwebsite.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manager")
public class AdminNewsController {

  public final NewsService newsService;

  @Autowired
  public AdminNewsController(NewsService newsService) {
    this.newsService = newsService;
  }

  /** Duyệt tin tức */
  @GetMapping("/accept-news/{id}")
  public String acceptNews(
      @PathVariable(name = "id") int newsId,
      @CookieValue(name = "admin", defaultValue = "0") int admin) {
    if (newsService.changeStateNews(newsId, NewsStateConstant.ACTIVE.name(), admin)) {
      return "redirect:/collaborator/news-management/page/1?text=accept-news";
    }
    return "redirect:/collaborator/news-management/page/1?text=not-exists";
  }
}
