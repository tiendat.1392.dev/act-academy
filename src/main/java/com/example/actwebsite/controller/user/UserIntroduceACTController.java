package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.MediaComponentService;
import com.example.actwebsite.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserIntroduceACTController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final NewsService newsService;

  @Autowired
  public UserIntroduceACTController(
      MediaComponentService mediaComponentService,
      AdminRepository adminRepository, NewsService newsService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.newsService = newsService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy trang Giới thiệu Học viện với mặc định là bài viết giới thiệu đầu tiên */
  @GetMapping("/introduce-act")
  public String getIntroduceACT(Model model) {
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    model.addAttribute("listIntroduceACT", newsService.findAllNewsIntroduce());
    model.addAttribute("content", newsService.findAllNewsIntroduce().get(0).getText());
    return "UserIntroduceACT";
  }

  /** Lấy trang Giới thiệu Học viện với bài viết giới thiệu được lựa chọn */
  @GetMapping("/introduce-act/{id}")
  public String getIntroduceACTById(Model model, @PathVariable(name = "id") int id) {
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    model.addAttribute("listIntroduceACT", newsService.findAllNewsIntroduce());
    model.addAttribute("content", newsService.findById(id).getText());
    return "UserIntroduceACT";
  }
}
