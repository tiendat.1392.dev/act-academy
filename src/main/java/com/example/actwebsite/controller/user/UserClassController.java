package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.OutputClass;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.MediaComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserClassController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final ClassService classService;

  @Autowired
  public UserClassController(
      MediaComponentService mediaComponentService,
      AdminRepository adminRepository, ClassService classService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.classService = classService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy danh sách các lớp học sắp khai giảng */
  @GetMapping("/list-class/page/{pageId}")
  public String getListClass(Model model, @PathVariable(name = "pageId") int pageId) {
    Page<OutputClass> listClasses =
        classService.getListClassPageable(
            new SearchRequest(null, null, ClassStateConstant.WAIT.name()), pageId, 4);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listClasses == null || listClasses.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listClasses", null);
    } else {
      int page = listClasses.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listClasses", listClasses);
    }
    return "UserListClass";
  }

  /** Lấy danh sách các lớp học sắp khai giảng theo phân loại (Lập trình, Mạng) */
  @GetMapping("/list-class/{type}/page/{pageId}")
  public String getListClassByType(
      Model model,
      @PathVariable(name = "pageId") int pageId,
      @PathVariable(name = "type") String type) {
    Page<OutputClass> listClasses =
        classService.getListClassPageable(
            new SearchRequest(null, type, ClassStateConstant.WAIT.name()), pageId, 4);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listClasses == null || listClasses.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listClasses", null);
    } else {
      int page = listClasses.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listClasses", listClasses);
    }
    return "UserListClass";
  }
}
