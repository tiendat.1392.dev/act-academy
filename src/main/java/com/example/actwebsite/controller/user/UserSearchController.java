package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.NewsStateConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.OutputCourse;
import com.example.actwebsite.model.dto.OutputJob;
import com.example.actwebsite.model.dto.OutputNews;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.CourseService;
import com.example.actwebsite.service.JobService;
import com.example.actwebsite.service.MediaComponentService;
import com.example.actwebsite.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserSearchController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final CourseService courseService;
  private final NewsService newsService;
  private final JobService jobService;

  @Autowired
  public UserSearchController(
      MediaComponentService mediaComponentService,
      AdminRepository adminRepository,
      CourseService courseService,
      NewsService newsService,
      JobService jobService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.courseService = courseService;
    this.newsService = newsService;
    this.jobService = jobService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy trang kết quả tìm kiếm theo phân loại (Khóa học, Tin tức, Việc làm) */
  @GetMapping("/search/type/{type}/page/{pageId}")
  public String getSearch(
      Model model,
      @ModelAttribute SearchRequest search,
      @PathVariable(name = "type") String type,
      @PathVariable(name = "pageId") int pageId) {
    String key = search.getContent();
    model.addAttribute("key", key);
    model.addAttribute("search", search);
    model.addAttribute("admin", getManager());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    long numberResult = 0;
    int size = 8;
    int page = 0;
    switch (type) {
      case "course":
        Page<OutputCourse> listCoursePageable =
            courseService.getListCoursePageable(new SearchRequest(key, null, null), pageId, size);
        if (listCoursePageable != null) {
          numberResult = listCoursePageable.getTotalElements();
          page = listCoursePageable.getTotalPages();
        }
        model.addAttribute("type", "course");
        model.addAttribute("listCourses", listCoursePageable);
        model.addAttribute("listNews", null);
        model.addAttribute("listJobs", null);
        break;
      case "news":
        Page<OutputNews> listNewsPageable =
            newsService.getListNewsPageable(
                new SearchRequest(key, null, NewsStateConstant.ACTIVE.name()),
                TypeConstant.Introduce.name(),
                pageId,
                size);
        if (listNewsPageable != null) {
          numberResult = listNewsPageable.getTotalElements();
          page = listNewsPageable.getTotalPages();
        }
        model.addAttribute("type", "news");
        model.addAttribute("listCourses", null);
        model.addAttribute("listNews", listNewsPageable);
        model.addAttribute("listJobs", null);
        break;
      case "job":
        Page<OutputJob> listJobPageable =
            jobService.getListJobPageable(new SearchRequest(key, null, null), pageId, size);
        if (listJobPageable != null) {
          numberResult = listJobPageable.getTotalElements();
          page = listJobPageable.getTotalPages();
        }
        model.addAttribute("type", "job");
        model.addAttribute("listCourses", null);
        model.addAttribute("listNews", null);
        model.addAttribute("listJobs", listJobPageable);
        break;
    }
    model.addAttribute("numberResult", numberResult);
    model.addAttribute("page", page);
    model.addAttribute("pageId", pageId);
    model.addAttribute("stt", Utils.getListNumberPage(page));
    return "UserSearch";
  }
}
