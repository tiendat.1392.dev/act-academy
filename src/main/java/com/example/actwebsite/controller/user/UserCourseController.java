package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.OutputCourse;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.CourseService;
import com.example.actwebsite.service.MediaComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserCourseController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final CourseService courseService;

  @Autowired
  public UserCourseController(
      MediaComponentService mediaComponentService,
      AdminRepository adminRepository,
      CourseService courseService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.courseService = courseService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy danh sách các khóa học */
  @GetMapping("/list-course/page/{pageId}")
  public String getListCourse(Model model, @PathVariable(name = "pageId") int pageId) {
    Page<OutputCourse> listCourses =
        courseService.getListCoursePageable(new SearchRequest(), pageId, 10);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("AllCourse", courseService.findAll());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listCourses == null || listCourses.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listCourse", null);
    } else {
      int page = listCourses.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listCourse", listCourses);
    }
    return "UserListCourse";
  }

  /** Lấy danh sách các khóa học theo phân loại (Lập trình, Mạng) */
  @GetMapping("/list-course/{type}/page/{pageId}")
  public String getListCourseByType(
      @PathVariable(name = "type") String type,
      @PathVariable(name = "pageId") int pageId,
      Model model) {
    Page<OutputCourse> listCourses =
        courseService.getListCoursePageable(new SearchRequest(null, type, null), pageId, 10);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("AllCourse", courseService.findAll());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listCourses == null || listCourses.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listCourse", null);
    } else {
      int page = listCourses.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listCourse", listCourses);
    }
    return "UserListCourse";
  }

  /** Lấy trang thông tin chi tiết khóa học */
  @GetMapping("/course/{id}")
  public String getCourse(@PathVariable(name = "id") int id, Model model) {
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("course", courseService.findById(id));
    model.addAttribute("AllCourse", courseService.findAll());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    return "UserDetailCourse";
  }
}
