package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.OutputJob;
import com.example.actwebsite.model.dto.OutputNews;
import com.example.actwebsite.model.dto.OutputUser;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.JobService;
import com.example.actwebsite.service.MediaComponentService;
import com.example.actwebsite.service.NewsService;
import com.example.actwebsite.service.UserService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserHomeController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final NewsService newsService;
  private final UserService userService;
  private final JobService jobService;

  @Autowired
  public UserHomeController(
      MediaComponentService mediaComponentService,
      AdminRepository adminRepository, NewsService newsService,
      UserService userService,
      JobService jobService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.newsService = newsService;
    this.userService = userService;
    this.jobService = jobService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /**
   * Lấy trang Home của người dùng (Học viên)
   */
  @GetMapping("/")
  public String getHomeUser(Model model) {
    Map<String, String> slider = mediaComponentService.getSlider();
    model.addAttribute("slider1", slider.get("slider_01"));
    model.addAttribute("slider2", slider.get("slider_02"));
    model.addAttribute("slider3", slider.get("slider_03"));
    model.addAttribute("slider4", slider.get("slider_04"));
    model.addAttribute("slider5", slider.get("slider_05"));
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    List<OutputNews> listNews = newsService.findLimit(4);
    model.addAttribute("listNews", listNews);
    List<OutputUser> listTopStudent = userService.findAllTopStudent();
    model.addAttribute("listTopStudent", listTopStudent);
    List<OutputJob> listJobs = jobService.findLimit(3);
    model.addAttribute("listJobs", listJobs);
    model.addAttribute("admin", getManager());
    return "UserHome";
  }
}
