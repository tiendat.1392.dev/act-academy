package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.InputUser;
import com.example.actwebsite.model.dto.OutputUsersClassAdmin;
import com.example.actwebsite.model.request.IdRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.CourseService;
import com.example.actwebsite.service.MediaComponentService;
import com.example.actwebsite.service.UserClassAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserRegisterController {

  private final MediaComponentService mediaComponentService;
  private final UserClassAdminService userClassAdminService;
  private final AdminRepository adminRepository;
  private final CourseService courseService;

  @Autowired
  public UserRegisterController(
      MediaComponentService mediaComponentService,
      UserClassAdminService userClassAdminService,
      AdminRepository adminRepository,
      CourseService courseService) {
    this.mediaComponentService = mediaComponentService;
    this.userClassAdminService = userClassAdminService;
    this.adminRepository = adminRepository;
    this.courseService = courseService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy trang Đăng ký học */
  @GetMapping("/register")
  public String getRegisterCourse(Model model, @ModelAttribute(name = "classId") int classId) {
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("user", new InputUser());
    model.addAttribute("course", new IdRequest());
    model.addAttribute("listCourse", courseService.findAll());
    model.addAttribute("classId", classId);
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    return "UserRegister";
  }

  /** Gửi thông tin đăng ký học */
  @PostMapping("/post-register")
  public String registerCourse(@ModelAttribute InputUser data, @ModelAttribute IdRequest courseId) {
    OutputUsersClassAdmin student =
        userClassAdminService.createStudentWithCourse(data, courseId.getId(), 0);
    int classId = student != null ? student.getClassCourse().getId() : -1;
    return "redirect:/user/register?classId=" + classId;
  }
}
