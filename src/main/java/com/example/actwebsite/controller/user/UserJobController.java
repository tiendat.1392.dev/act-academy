package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.InputJob;
import com.example.actwebsite.model.dto.OutputJob;
import com.example.actwebsite.model.request.NotificationRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.JobService;
import com.example.actwebsite.service.MediaComponentService;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserJobController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final JobService jobService;

  @Autowired
  public UserJobController(
      MediaComponentService mediaComponentService,
      AdminRepository adminRepository,
      GenericMapper genericMapper,
      JobService jobService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.genericMapper = genericMapper;
    this.jobService = jobService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy trang danh sách các tin tuyển dụng */
  @GetMapping("/list-job/page/{pageId}")
  public String getListJob(
      Model model,
      @ModelAttribute NotificationRequest notification,
      @PathVariable(name = "pageId") int pageId) {
    if (notification.getText() != null) {
      switch (notification.getText()) {
        case "not-exists":
          notification.setText("Tin tuyển dụng không tồn tại");
          break;
        case "create-success":
          notification.setText(
              "Đăng tin tuyển dụng thành công! <br> Chúc quý công ty sớm tìm được ứng viên tốt nhất");
          break;
        case "delete-success":
          notification.setText(
              "Xóa tin tuyển dụng thành công! <br> Học viện ACT rất mong chờ những tin tuyển dụng khác từ phía quý công ty");
          break;
        default:
          notification.setText(null);
          break;
      }
    }
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("notification", notification);
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    Page<OutputJob> listJobs = jobService.getListJobPageable(new SearchRequest(), pageId, 10);
    if (listJobs == null || listJobs.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listJobs", null);
    } else {
      int page = listJobs.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listJobs", listJobs);
    }
    return "UserListJob";
  }

  /** Lấy trang danh sách các tin tuyển dụng theo phân loại */
  @GetMapping("/list-job/{type}/page/{pageId}")
  public String getListJobByType(
      Model model,
      @PathVariable(name = "type") String type,
      @PathVariable(name = "pageId") int pageId) {
    Page<OutputJob> listJobs =
        jobService.getListJobPageable(new SearchRequest(null, type, null), pageId, 10);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("notification", null);
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listJobs == null || listJobs.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listJobs", null);
    } else {
      int page = listJobs.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listJobs", listJobs);
    }
    return "UserListJob";
  }

  /** Lấy trang thông tin chi tiết tin tuyển dụng */
  @GetMapping("/job/{id}")
  public String getJob(
      Model model,
      @PathVariable(name = "id") int jobId,
      @ModelAttribute NotificationRequest notification,
      @CookieValue(name = "job", defaultValue = "0") int job) {
    OutputJob jobInfor = jobService.findById(jobId);
    if (jobInfor != null) {
      model.addAttribute("login", "false");
      if (job != 0 && job == jobId) {
        model.addAttribute("login", "true");
      }
      if (notification.getText() != null) {
        switch (notification.getText()) {
          case "login-null":
            notification.setText("Bạn chưa đăng nhập để chỉnh sửa tin tuyển dụng");
            break;
          case "login-success":
            notification.setText("Đăng nhập thành công");
            break;
          case "login-fail":
            notification.setText("Đăng nhập thất bại<br> Xin hãy kiểm tra lại mật khẩu bạn nhập");
            break;
          case "change-password-success":
            notification.setText(
                "Mật khẩu mới đã được gửi vào email của người đăng<br> Xin hãy kiểm tra email");
            break;
          case "update-success":
            notification.setText("Cập nhật chỉnh sửa thông tin tuyển dụng thành công");
            break;
          default:
            notification.setText(null);
            break;
        }
      }
      model.addAttribute("job", jobInfor);
      model.addAttribute("admin", getManager());
      model.addAttribute("notification", notification);
      model.addAttribute("search", new SearchRequest());
      model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
      return "UserDetailJob";
    } else {
      return "redirect:/user/list-job/page/1?text=not-exists";
    }
  }

  /** Đăng nhập để chỉnh sửa, xóa tin tuyển dụng */
  @GetMapping("/job/{id}/login/{password}")
  public String loginPosterJob(
      HttpServletResponse response,
      @PathVariable(name = "id") int jobId,
      @PathVariable(name = "password") String password) {
    boolean login = jobService.loginJob(jobId, password);
    String notification;
    if (login) {
      Cookie cookie = new Cookie("job", String.valueOf(jobId));
      cookie.setMaxAge(24 * 60 * 60);
      cookie.setPath("/user/job");
      cookie.setHttpOnly(true);
      response.addCookie(cookie);
      notification = "login-success";
    } else {
      notification = "login-fail";
    }
    return "redirect:/user/job/" + jobId + "?text=" + notification;
  }

  /** Lấy lại mật khẩu để đăng nhập chỉnh sửa, xóa tin tuyển dụng */
  @GetMapping("/job/{id}/forget-password")
  public String jobForgetPassword(@PathVariable(name = "id") int jobId) {
    if (jobService.forgetPassword(jobId)) {
      return "redirect:/user/job/" + jobId + "?text=change-password-success";
    }
    return "redirect:/user/list-job/page/1?text=not-exists";
  }

  /** Lấy trang đăng tin tuyển dụng */
  @GetMapping("/posting-job")
  public String postingJob(Model model) {
    model.addAttribute("admin", getManager());
    model.addAttribute("job", new InputJob());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    return "UserPostingJob";
  }

  /** Lấy trang chỉnh sửa tin tuyển dụng */
  @GetMapping("/job/{id}/get-update-job")
  public String getUpdateJob(
      Model model,
      @PathVariable(name = "id") int jobId,
      @CookieValue(name = "job", defaultValue = "0") int job) {
    if (job == 0 || job != jobId) {
      String notification = "login-null";
      return "redirect:/user/job/" + jobId + "?text=" + notification;
    }
    model.addAttribute("jobId", jobId);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    model.addAttribute("job", genericMapper.mapToType(jobService.findById(jobId), InputJob.class));
    return "UserUpdateJob";
  }

  /** Đăng tin tuyển dụng */
  @PostMapping("/create-job")
  public String createJob(@ModelAttribute InputJob data) {
    jobService.createJob(data);
    return "redirect:/user/list-job/page/1?text=create-success";
  }

  /** Chỉnh sửa tin tuyển dụng */
  @PostMapping("/update-job/{id}")
  public String updateJob(@ModelAttribute InputJob data, @PathVariable(name = "id") int jobId) {
    if (jobService.updateJob(jobId, data)) {
      return "redirect:/user/job/" + jobId + "?text=update-success";
    }
    return "redirect:/user/list-job/page/1?text=not-exists";
  }

  /** Xóa tin tuyển dụng */
  @GetMapping("/job/{id}/delete")
  public String deleteJob(
      @PathVariable(name = "id") int jobId,
      @CookieValue(name = "job", defaultValue = "0") int job) {
    if (job == 0 || job != jobId) {
      String notification = "login-null";
      return "redirect:/user/job/" + jobId + "?text=" + notification;
    }
    if (jobService.deleteJob(jobId, null)) {
      return "redirect:/user/list-job/page/1?text=delete-success";
    }
    return "redirect:/user/list-job/page/1?text=not-exists";
  }
}
