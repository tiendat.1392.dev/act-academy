package com.example.actwebsite.controller.user;

import com.example.actwebsite.common.enums.NewsStateConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.OutputNews;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.MediaComponentService;
import com.example.actwebsite.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserNewsController {

  private final MediaComponentService mediaComponentService;
  private final AdminRepository adminRepository;
  private final NewsService newsService;

  @Autowired
  public UserNewsController(MediaComponentService mediaComponentService,
      AdminRepository adminRepository, NewsService newsService) {
    this.mediaComponentService = mediaComponentService;
    this.adminRepository = adminRepository;
    this.newsService = newsService;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  /** Lấy trang danh sách các tin tức */
  @GetMapping("/list-news/page/{pageId}")
  public String getListNews(Model model, @PathVariable(name = "pageId") int pageId) {
    Page<OutputNews> listNews =
        newsService.getListNewsPageable(
            new SearchRequest(null, null, NewsStateConstant.ACTIVE.name()),
            TypeConstant.Introduce.name(),
            pageId,
            4);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listNews == null || listNews.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listNews", null);
    } else {
      int page = listNews.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listNews", listNews);
    }
    return "UserListNews";
  }

  /** Lấy trang danh sách tin tức theo phân loại (Học viện, Công nghệ) */
  @GetMapping("/list-news/{type}/page/{pageId}")
  public String getListNewsByType(
      Model model,
      @PathVariable(name = "type") String type,
      @PathVariable(name = "pageId") int pageId) {
    Page<OutputNews> listNews =
        newsService.getListNewsPageable(
            new SearchRequest(null, type, NewsStateConstant.ACTIVE.name()), null, pageId, 4);
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    if (listNews == null || listNews.isEmpty()) {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listNews", null);
    } else {
      int page = listNews.getTotalPages();
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", Utils.getListNumberPage(page));
      model.addAttribute("listNews", listNews);
    }
    return "UserListNews";
  }

  /** Lấy trang thông tin chi tiết tin tức */
  @GetMapping("/news/{id}")
  public String getNews(@PathVariable(name = "id") int id, Model model) {
    model.addAttribute("admin", getManager());
    model.addAttribute("search", new SearchRequest());
    model.addAttribute("News", newsService.findById(id));
    model.addAttribute("video_banner", mediaComponentService.getUrlVideoBanner());
    return "UserDetailNews";
  }
}
