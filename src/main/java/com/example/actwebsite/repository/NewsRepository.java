package com.example.actwebsite.repository;

import com.example.actwebsite.entity.News;
import com.example.actwebsite.model.projection.NewsStatisticProjection;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NewsRepository extends JpaRepository<News, Integer> {

  Optional<News> findByIdAndStateNot(Integer integer, String stateDeleted);

  Optional<News> findByNameAndStateNot(String name, String stateDeleted);

  Optional<News> findByIdNotAndNameAndStateNot(int newsId, String name, String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(n) FROM News n "
              + "WHERE (:search IS NULL OR (n.name LIKE :search OR n.poster.name LIKE :search)) "
              + "AND (:type IS NULL OR n.type = :type) "
              + "AND (:state IS NULL OR n.state = :state) "
              + "AND n.state <> :stateDeleted")
  List<News> findAllByNameOrCreateByAndTypeAndStateAndStateNot(
      @Param(value = "search") String search,
      @Param(value = "type") String type,
      @Param(value = "state") String state,
      @Param(value = "stateDeleted") String stateDeleted,
      Sort sort);

  Page<News> findAllByTypeNotAndState(String type, String state, Pageable pageable);

  List<News> findAllByTypeAndStateNot(String type, String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(n) FROM News n "
              + "WHERE (:search IS NULL OR (n.name LIKE :search OR n.source LIKE :search)) "
              + "AND (:type IS NULL OR n.type = :type) "
              + "AND (:typeNot IS NULL OR n.type <> :typeNot) "
              + "AND (:state IS NULL OR n.state = :state) "
              + "AND n.state <> :stateDeleted")
  Page<News> findAllByNameOrSourceAndTypeAndState(
      @Param(value = "search") String search,
      @Param(value = "type") String type,
      @Param(value = "state") String state,
      @Param(value = "typeNot") String typeNot,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);

  List<News> findAllByStateNot(String stateDeleted, Sort sort);

  @Query(
      value =
          "SELECT n.state as state, n.type as type, COALESCE(COUNT(n), 0) AS numberNews "
              + "FROM News n WHERE "
              + "(:dateStart IS NULL OR n.createdDate >= :dateStart) "
              + "AND (:dateEnd IS NULL OR n.createdDate <= :dateEnd) "
              + "AND n.state <> :stateDeleted "
              + "GROUP BY n.state, n.type")
  List<NewsStatisticProjection> countNumberNewsByStateAndType(
      @Param(value = "dateStart") LocalDate dateStart,
      @Param(value = "dateEnd") LocalDate dateEnd,
      @Param(value = "stateDeleted") String stateDeleted);
}
