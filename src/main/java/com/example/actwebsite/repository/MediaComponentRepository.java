package com.example.actwebsite.repository;

import com.example.actwebsite.entity.MediaComponents;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaComponentRepository extends JpaRepository<MediaComponents, Integer> {

  Optional<MediaComponents> findByName(String name);

  List<MediaComponents> findAllByNameNot(String name);
}
