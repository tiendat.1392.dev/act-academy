package com.example.actwebsite.repository;

import com.example.actwebsite.entity.Course;
import com.example.actwebsite.model.projection.CourseStatisticProjection;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CourseRepository extends JpaRepository<Course, Integer> {

  Optional<Course> findByIdAndStateNot(Integer integer, String stateDeleted);

  Optional<Course> findByNameAndStateNot(String name, String stateDeleted);

  Optional<Course> findByIdNotAndNameAndStateNot(int id, String name, String stateDeleted);

  List<Course> findAllByStateNot(String stateDeleted, Sort sort);

  @Query(
      value =
          "SELECT c.type as type, COALESCE(COUNT(c), 0) as numberCourse "
              + "FROM Course c WHERE "
              + "(:dateStart IS NULL OR c.createdDate >= :dateStart) "
              + "AND (:dateEnd IS NULL OR c.createdDate <= :dateEnd) "
              + "AND c.state <> :stateDeleted "
              + "GROUP BY c.type")
  List<CourseStatisticProjection> countNumberCourseByType(
      @Param(value = "dateStart") LocalDate dateStart,
      @Param(value = "dateEnd") LocalDate dateEnd,
      @Param(value = "stateDeleted") String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Course c "
              + "WHERE (:search IS NULL OR (c.name LIKE :search)) "
              + "AND (:type IS NULL OR c.type = :type) "
              + "AND c.state <> :stateDeleted")
  Page<Course> findAllByNameAndTypeAndState(
      @Param(value = "search") String search,
      @Param(value = "type") String type,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);
}
