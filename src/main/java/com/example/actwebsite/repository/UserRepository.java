package com.example.actwebsite.repository;

import com.example.actwebsite.entity.User;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Integer> {

  @Query(
      value =
          "SELECT COALESCE(COUNT(u.id), 0) as numberStudent FROM User u WHERE u.state <> :stateDeleted")
  Long countNumberTopStudentAndStateNot(@Param(value = "stateDeleted") String stateDeleted);

  @Query(
      value =
          "SELECT COALESCE(COUNT(u.id), 0) as numberStudent "
              + "FROM User u "
              + "WHERE (:dateStart IS NULL OR u.createdDate >= :dateStart) "
              + "  AND (:dateEnd IS NULL OR u.createdDate <= :dateEnd) "
              + "  And u.state <> :stateDeleted ")
  Long countAllByStateNot(
      @Param(value = "dateStart") LocalDate dateStart,
      @Param(value = "dateEnd") LocalDate dateEnd,
      String stateDeleted);

  Optional<User> findByIdAndStateNot(Integer id, String stateDeleted);

  Optional<User> findByEmailAndStateNot(String email, String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(u) FROM User u WHERE u.id <> :userId "
              + "AND (u.email = :email) "
              + "AND u.state <> :stateDeleted")
  Optional<User> findUserByIdNotAndEmailAndStateNot(
      @Param(value = "userId") int userId,
      @Param(value = "email") String email,
      @Param(value = "stateDeleted") String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(u) FROM User u "
              + "WHERE (:search IS NULL OR u.name LIKE :search OR u.email LIKE :search OR u.phone LIKE :search) "
              + "AND u.state <> :stateDeleted")
  List<User> findAllByNameOrEmailOrPhoneAndStateNot(
      @Param(value = "search") String search,
      @Param(value = "stateDeleted") String stateDeleted,
      Sort sort);

  List<User> findAllByStateNot(String stateDeleted, Sort sort);

  List<User> findAllByTopStudentAndStateNot(Boolean topStudent, String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(u) FROM User u "
              + "WHERE (:search IS NULL OR (u.name LIKE :search OR u.email LIKE :search OR u.phone LIKE :search)) "
              + "AND (:topStudent IS NULL OR u.topStudent = :topStudent) "
              + "AND u.state <> :stateDeleted")
  Page<User> findAllByNameOrEmailOrPhoneAndStateNot(
      @Param(value = "search") String search,
      @Param(value = "topStudent") Boolean topStudent,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);
}
