package com.example.actwebsite.repository;

import com.example.actwebsite.entity.Class;
import com.example.actwebsite.model.projection.ClassStatisticProjection;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClassRepository extends JpaRepository<Class, Integer> {

  Optional<Class> findByCourseIdAndState(Integer courseId, String state);

  Optional<Class> findByIdAndStateNot(int id, String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Class c WHERE c.id <> :classId "
              + "AND (c.name = :name OR c.fullName = :fullName) "
              + "AND c.state <> :stateDeleted")
  Optional<Class> findClassByIdNotAndNameOrFullNameAndStateNot(
      @Param(value = "classId") int classId,
      @Param(value = "name") String name,
      @Param(value = "fullName") String fullName,
      @Param(value = "stateDeleted") String stateDeleted);

  List<Class> findAllByStateNot(String stateDeleted, Sort sort);

  @Query(
      value =
          "SELECT c.state as state, COALESCE(COUNT(c), 0) as numberClass "
              + "FROM Class c WHERE "
              + "(:dateStart IS NULL OR c.createdDate >= :dateStart) "
              + "AND (:dateEnd IS NULL OR c.createdDate <= :dateEnd) "
              + "And c.state <> :stateDeleted "
              + "GROUP BY c.state")
  List<ClassStatisticProjection> countNumberClassByState(
      @Param(value = "dateStart") LocalDate dateStart,
      @Param(value = "dateEnd") LocalDate dateEnd,
      @Param(value = "stateDeleted") String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(c) FROM Class c "
              + "WHERE (:search IS NULL OR c.name LIKE :search OR c.teacherName LIKE :search OR c.manager.name LIKE :search) "
              + "AND (:state IS NULL OR c.state = :state) "
              + "AND c.state <> :stateDeleted")
  Page<Class> findAllByNameOrTeacherNameOrSupporterNameOrManagerNameAndTypeAndState(
      @Param(value = "search") String search,
      @Param(value = "state") String state,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);

  @Query(
      value =
          "SELECT c FROM Class c WHERE "
              + "(c.supporter.id = :collaboratorId OR c.manager.id = :collaboratorId) "
              + "And c.state <> :stateDeleted ")
  Page<Class> findAllByCollaboratorIdAndStateNot(
      @Param(value = "collaboratorId") int collaboratorId,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);
}
