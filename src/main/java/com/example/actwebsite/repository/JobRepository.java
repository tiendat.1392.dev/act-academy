package com.example.actwebsite.repository;

import com.example.actwebsite.entity.Job;
import com.example.actwebsite.model.projection.JobStatisticProjection;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobRepository extends JpaRepository<Job, Integer> {

  Optional<Job> findByIdAndPassword(Integer jobId, String password);

  @Query(
      value =
          "SELECT DISTINCT(j) FROM Job j "
              + "WHERE (:search IS NULL OR (j.name LIKE :search OR j.contact LIKE :search OR j.phone LIKE :search OR j.email LIKE :search)) "
              + "AND (:type IS NULL OR j.type = :type)")
  Page<Job> findAllByNameOrContactOrPhoneOrEmailAndType(
      @Param(value = "search") String search,
      @Param(value = "type") String type,
      Pageable pageable);

  @Query(
      value =
          "SELECT j.type as type, COALESCE(COUNT(j), 0) as numberJob "
              + "FROM Job j WHERE "
              + "(:dateStart IS NULL OR j.datePosted >= :dateStart) "
              + "AND (:dateEnd IS NULL OR j.datePosted <= :dateEnd) "
              + "GROUP BY j.type")
  List<JobStatisticProjection> countNumberJobByType(
      @Param(value = "dateStart") LocalDate dateStart, @Param(value = "dateEnd") LocalDate dateEnd);
}
