package com.example.actwebsite.repository;

import com.example.actwebsite.entity.UserClassAdmin;
import com.example.actwebsite.model.projection.StudentStatisticProjection;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserClassAdminRepository extends JpaRepository<UserClassAdmin, Integer> {

  Optional<UserClassAdmin> findByClassCourseIdAndStudentIdAndStateNot(
      int classId, int studentId, String stateDeleted);

  @Query(
      value =
          "SELECT uca.classCourse                 as classCourse, "
              + " uca.state                       as state, "
              + " COALESCE(COUNT(uca.student), 0) as numberStudent "
              + "FROM UserClassAdmin uca "
              + "WHERE (:dateStart IS NULL OR uca.createdDate >= :dateStart) "
              + "  AND (:dateEnd IS NULL OR uca.createdDate <= :dateEnd) "
              + "  And uca.state <> :stateDeleted "
              + "GROUP BY uca.classCourse, uca.state")
  List<StudentStatisticProjection> countNumberStudentByState(
      @Param(value = "dateStart") LocalDate dateStart,
      @Param(value = "dateEnd") LocalDate dateEnd,
      @Param(value = "stateDeleted") String stateDeleted);

  List<UserClassAdmin> findAllByStudentIdAndStateNot(int studentId, String stateDeleted);

  Page<UserClassAdmin> findAllByClassCourseIdAndStateNot(int classId, String stateDeleted, Pageable pageable);

  @Query(
      value =
          "SELECT DISTINCT(uca) "
              + "FROM UserClassAdmin uca "
              + "WHERE uca.classCourse.id = :classId "
              + "  AND (:search IS NULL OR uca.student.name LIKE :search "
              + "    OR uca.student.email LIKE :search OR uca.student.phone LIKE :search) "
              + "  AND (:state IS NULL OR uca.state = :state) "
              + "  AND uca.state <> :stateDeleted")
  Page<UserClassAdmin>
  findAllByClassIdAndStudentNameOrStudentEmailOrStudentPhoneAndState(
      @Param(value = "classId") int classId,
      @Param(value = "search") String search,
      @Param(value = "state") String state,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);

  Page<UserClassAdmin> findAllByStudentIdAndStateNot(
      int studentId, String stateDeleted, Pageable pageable);
}
