package com.example.actwebsite.repository;

import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.projection.AdminStatisticProjection;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdminRepository extends JpaRepository<Admin, Integer> {

  Optional<Admin> findByIdAndStateNot(int id, String stateDeleted);

  Optional<Admin> findByRoleAndStateNot(String role, String stateDeleted);

  Optional<Admin> findByEmailAndStateNot(String email, String stateDeleted);

  Optional<Admin> findByIdNotAndEmailAndStateNot(int id, String email, String stateDeleted);

  Optional<Admin> findByIdAndPasswordAndStateNot(int id, String password, String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(a) FROM Admin a "
              + "WHERE (:search IS NULL OR a.name LIKE :search OR a.email LIKE :search OR a.phone LIKE :search) "
              + "AND a.state <> :stateDeleted")
  List<Admin> findAllByNameOrEmailOrPhoneAndStateNot(
      @Param(value = "search") String search,
      @Param(value = "stateDeleted") String stateDeleted,
      Sort sort);

  List<Admin> findAllByRoleNotAndStateNot(String role, String stateDeleted, Sort sort);

  @Query(
      value =
          "SELECT a.role as role, COALESCE(COUNT(a), 0) as numberCollaborator "
              + "FROM Admin a WHERE "
              + "(:dateStart IS NULL OR a.createdDate >= :dateStart) "
              + "AND (:dateEnd IS NULL OR a.createdDate <= :dateEnd) "
              + "AND a.state <> :stateDeleted "
              + "GROUP BY a.role")
  List<AdminStatisticProjection> countNumberCollaboratorByRole(
      @Param(value = "dateStart") LocalDate dateStart,
      @Param(value = "dateEnd") LocalDate dateEnd,
      @Param(value = "stateDeleted") String stateDeleted);

  @Query(
      value =
          "SELECT DISTINCT(a) FROM Admin a "
              + "WHERE (:search IS NULL OR (a.name LIKE :search OR a.email LIKE :search OR a.phone LIKE :search)) "
              + "AND a.role = :role "
              + "AND a.state <> :stateDeleted")
  Page<Admin> findAllByNameOrEmailOrPhoneAndStateNot(
      @Param(value = "search") String search,
      @Param(value = "role") String role,
      @Param(value = "stateDeleted") String stateDeleted,
      Pageable pageable);
}
