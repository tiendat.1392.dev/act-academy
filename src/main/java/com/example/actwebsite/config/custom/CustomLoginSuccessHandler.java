package com.example.actwebsite.config.custom;

import com.example.actwebsite.model.dto.OutputAdmin;
import com.example.actwebsite.service.AdminService;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class CustomLoginSuccessHandler implements AuthenticationSuccessHandler {

  @Autowired private AdminService adminService;
  @Autowired private RedirectStrategy redirectStrategy;

  @Override
  @SneakyThrows
  public void onAuthenticationSuccess(
      HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    UserDetails userDetails = (UserDetails) authentication.getPrincipal();
    OutputAdmin admin = adminService.findByEmail(userDetails.getUsername());
    Cookie cookie = new Cookie("admin", String.valueOf(admin.getId()));
    cookie.setMaxAge(24 * 60 * 60);
    cookie.setHttpOnly(true);
    response.addCookie(cookie);
    redirectStrategy.sendRedirect(request, response, "/collaborator/home-statistic");
  }
}
