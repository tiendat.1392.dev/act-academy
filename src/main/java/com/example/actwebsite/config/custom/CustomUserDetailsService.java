package com.example.actwebsite.config.custom;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.repository.AdminRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

  private final AdminRepository adminRepository;

  @Autowired
  public CustomUserDetailsService(AdminRepository adminRepository) {
    this.adminRepository = adminRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Admin admin =
        adminRepository.findByEmailAndStateNot(username, StateConstant.DELETED.name()).orElse(null);
    if (admin != null) {
      if (admin.getRole().equals(RoleConstant.Owner.name())) {
        admin.setRole(RoleConstant.Manager.name());
      }
      List<GrantedAuthority> grantList = new ArrayList<>();
      GrantedAuthority authority = new SimpleGrantedAuthority(admin.getRole());
      grantList.add(authority);
      return new User(admin.getEmail(), admin.getPassword(), grantList);
    } else {
      new UsernameNotFoundException("Login fail");
    }
    return null;
  }
}
