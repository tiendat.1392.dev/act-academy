package com.example.actwebsite.config;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.config.custom.CustomLoginSuccessHandler;
import com.example.actwebsite.config.custom.CustomUserDetailsService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final CustomUserDetailsService customUserDetailsService;

  @Autowired
  public SecurityConfig(CustomUserDetailsService customUserDetailsService) {
    this.customUserDetailsService = customUserDetailsService;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public RedirectStrategy redirectStrategy() {
    return new DefaultRedirectStrategy();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .disable()
        .authorizeRequests()
        .antMatchers("/", "/user/**", "/static/**", "/login-admin", "/forget-password/**")
        .permitAll()
        .antMatchers("/collaborator/**")
        .hasAnyAuthority(RoleConstant.Manager.name(), RoleConstant.Collaborator.name())
        .antMatchers("/manager/**")
        .hasAnyAuthority(RoleConstant.Manager.name())
        .anyRequest()
        .authenticated()
        .and()
        .formLogin()
        .loginPage("/login-admin")
        .permitAll()
        .usernameParameter("email")
        .passwordParameter("password")
        .loginProcessingUrl("/login")
        .failureUrl("/login-admin?text=login-fail")
        .successHandler(customLoginSuccessHandler())
        .and()
        .logout()
        .logoutUrl("/logout")
        .permitAll()
        .deleteCookies("JSESSIONID", "admin");
  }

  @Override
  @SneakyThrows
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
  }

  @Bean
  protected CustomLoginSuccessHandler customLoginSuccessHandler() {
    return new CustomLoginSuccessHandler();
  }
}
