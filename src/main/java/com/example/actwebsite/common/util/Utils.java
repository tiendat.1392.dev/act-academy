package com.example.actwebsite.common.util;

import com.example.actwebsite.common.enums.RoleConstant;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class Utils {

  public static String formatFileName(String fileName) {
    String temp = Normalizer.normalize(fileName, Normalizer.Form.NFD);
    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    String name =
        pattern
            .matcher(temp)
            .replaceAll("")
            .toLowerCase()
            .replaceAll(" ", "_")
            .replaceAll("đ", "d");
    return name.toLowerCase() + "_" + System.currentTimeMillis();
  }

  public static String formatParameterQueryLike(String str) {
    return str == null || str.trim().isEmpty() ? null : "%" + str + "%";
  }

  public static String formatParameterQueryEqual(String str) {
    return str == null || str.trim().isEmpty() ? null : str;
  }

  public static boolean stringIsNull(String str) {
    return str == null || str.trim().isEmpty();
  }

  public static void refreshCookie(HttpServletResponse response, HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      Arrays.stream(cookies)
          .forEach(
              cookie -> {
                cookie.setValue("");
                cookie.setPath("/");
                cookie.setMaxAge(0);
                response.addCookie(cookie);
              });
    }
  }

  public static List<Integer> getListNumberPage(int page) {
    List<Integer> stt = new ArrayList<>();
    for (int i = 1; i <= page; i++) {
      stt.add(i);
    }
    return stt;
  }

  public static Boolean checkRoleAdmin() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    return auth.getAuthorities().stream()
        .anyMatch(a -> a.getAuthority().equals(RoleConstant.Manager.name()));
  }
}
