package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AdminInfoConstant {
  Name("Trần Thúy Hường"),
  Email("daide1392@gmail.com");
  private String value;
}
