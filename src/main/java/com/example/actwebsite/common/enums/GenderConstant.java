package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GenderConstant {
  Male("Nam"),
  Female("Nữ");
  private final String value;
}
