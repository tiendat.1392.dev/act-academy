package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ImgConstant {
  Prefix("https://res.cloudinary.com/dralhpdiv/image/upload/"),
  UnknownUser(
      "https://res.cloudinary.com/dralhpdiv/image/upload/v1649214207/admin_user/unknow_user.jpg"),
  UnknownJob(
      "https://res.cloudinary.com/dralhpdiv/image/upload/c_thumb,w_200,g_face/v1643096968/job/unknow_job.png"),
  UnknownCourse(
      "https://res.cloudinary.com/dralhpdiv/image/upload/w_1000,ar_1:1,c_fill,g_auto,e_art:hokusai/v1632797577/course/unknow_course.png"),
  UnknownNews(
      "https://res.cloudinary.com/dralhpdiv/image/upload/c_scale,h_162,w_260/v1646968828/news/unknow_news.jpg");
  private String value;
}
