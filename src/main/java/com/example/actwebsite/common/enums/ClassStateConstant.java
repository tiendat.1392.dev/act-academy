package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ClassStateConstant {
  PREP("Dự bị"),
  WAIT("Sắp khai giảng"),
  DOING("Đang diễn ra"),
  FINISHED("Đã kết thúc"),
  DELETED("Đã xóa");
  private String value;
}
