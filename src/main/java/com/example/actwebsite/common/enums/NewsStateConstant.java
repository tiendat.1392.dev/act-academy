package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NewsStateConstant {
  ACTIVE("Hoạt động"),
  WAIT("Chờ duyệt"),
  DELETED("Đã xóa");
  private String value;
}
