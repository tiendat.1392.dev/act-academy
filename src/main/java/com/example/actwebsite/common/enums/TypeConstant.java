package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeConstant {
  Programming("Lập trình"),
  Network("Mạng hệ thống"),
  Academy("Học viện"),
  Tech("Công nghệ"),
  Introduce("Giới thiệu học viện");
  private String value;
}
