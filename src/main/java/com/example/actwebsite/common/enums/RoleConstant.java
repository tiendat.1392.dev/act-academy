package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleConstant {
  Owner("Giám đốc học viện"),
  Manager("Quản lý"),
  Collaborator("Cộng tác viên");
  private String value;
}
