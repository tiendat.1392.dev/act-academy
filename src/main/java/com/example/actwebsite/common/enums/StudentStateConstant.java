package com.example.actwebsite.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StudentStateConstant {
  NEW("Mới đăng kí"),
  WAIT("Chưa đóng học"),
  PAY_HALF("Đã đóng một phần"),
  CONFIRM("Đã đóng học"),
  DELETED("Đã xóa");
  private String value;
}
