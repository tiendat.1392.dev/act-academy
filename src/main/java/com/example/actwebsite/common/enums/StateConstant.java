package com.example.actwebsite.common.enums;

public enum StateConstant {
  // Hoạt động
  ACTIVE,
  // Xóa
  DELETED,
}
