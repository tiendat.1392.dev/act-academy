package com.example.actwebsite.service;

public interface EmailService {

  /** Gửi email cho Admin thông báo có học viên mới */
  void sendEmailHaveNewStudent(String name, String phone, String email, String courseName);

  /** Gửi email cho Admin thông báo có yêu cầu lấy lại mật khẩu */
  void sendEmailCollaboratorForgetPassword(String email, String password);

  /** Gửi email thông báo cho Quản lý mới */
  void sendEmailNewAdmin(String name, String email);

  /** Gửi email chứa password mới dùng để chỉnh sửa tin tuyển dụng cho Người đăng */
  void sendEmailPosterNewPassword(String email, String password);

  /** Gửi email thông báo lý do tin tuyển dụng bị xóa cho Người đăng */
  void sendEmailPosterDeleteJob(String email, String jobName, String reason);
}
