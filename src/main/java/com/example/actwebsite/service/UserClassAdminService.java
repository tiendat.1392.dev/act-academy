package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputUser;
import com.example.actwebsite.model.dto.OutputStudentStatistic;
import com.example.actwebsite.model.dto.OutputUsersClassAdmin;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import org.springframework.data.domain.Page;

public interface UserClassAdminService {

  /** Lấy danh sách Học viên theo ID Lớp học */
  Page<OutputUsersClassAdmin> findByClassId(int classId, SearchRequest search, int page, int size);

  /** Lấy danh sách Lớp học theo ID Học viên */
  Page<OutputUsersClassAdmin> findByStudentId(int studentId, int page, int size);

  /** Đếm số lượng Học viên theo trạng thái của từng Lớp học */
  List<OutputStudentStatistic> countNumberStudentByState(StatisticTimeRequest statisticTimeRequest);

  /** Tạo học viên theo ID Khóa học */
  OutputUsersClassAdmin createStudentWithCourse(InputUser data, int courseId, int collaboratorId);

  /** Tạo học viên theo ID Lớp học */
  boolean createStudentWithClass(InputUser data, int classId, int collaboratorId);

  /** Thêm học viên vào Lớp học theo email */
  boolean addStudentToClassByEmail(int classId, String email, int adminId);

  /** Thay đổi trạng thái Học viên trong Lớp học */
  boolean changeStateStudentInClass(int classId, int studentId, String state, int adminId);

  /** Xóa học viên trong Lớp học */
  boolean deleteStudent(int classId, int studentId, int adminId);
}
