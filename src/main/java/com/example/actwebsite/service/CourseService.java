package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputCourse;
import com.example.actwebsite.model.dto.OutputCourse;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;

public interface CourseService {

  /** Lấy danh sách tất cả Khóa học */
  List<OutputCourse> findAll();

  /** Lấy danh sách tất cả Khóa học có phân trang và tìm kiếm theo yêu cầu */
  Page<OutputCourse> getListCoursePageable(SearchRequest search, int page, int size);

  /** Đếm số lượng Khóa học theo phân loại (Lập trình, Mạng) */
  Map<String, Long> countNumberCourseByType(StatisticTimeRequest statisticTimeRequest);

  /** Tìm khóa học theo ID */
  OutputCourse findById(int id);

  /** Tạo Khóa học */
  boolean createCourse(InputCourse data, int adminId);

  /** Xóa khóa học */
  boolean deleteCourse(int id, int adminId);

  /** Cập nhật thông tin Khóa học */
  boolean updateCourse(int courseId, InputCourse data, int adminId);
}
