package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputNews;
import com.example.actwebsite.model.dto.OutputNews;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;

public interface NewsService {

  /** Tìm tin tức theo ID */
  OutputNews findById(int id);

  /** Lấy danh sách Tin tức có giới hạn */
  List<OutputNews> findLimit(int limit);

  /** Lấy danh sách tất cả Tin tức */
  List<OutputNews> findAll();

  /** Lấy danh sách tất cả Tin tức thuộc Bài viết giới thiệu Học viện (dùng cho Introduce ACT) */
  List<OutputNews> findAllNewsIntroduce();

  /** Lấy danh sách tất cả Tin tức có phân trang và tìm kiếm theo yêu cầu */
  Page<OutputNews> getListNewsPageable(SearchRequest search, String typeNot, int page, int size);

  /** Đếm số lượng Tin tức theo Trạng thái và Phân loại */
  Map<String, Long> countNumberNewsByStateAndType(StatisticTimeRequest statisticTimeRequest);

  /** Tạo tin tức */
  boolean createNews(InputNews data, int adminId);

  /** Cập nhật tin tức */
  boolean updateNews(int newsId, InputNews data, int adminId);

  /** Đổi trạng thái tin tức (dùng cho Xóa tin tức và Duyệt tin tức) */
  boolean changeStateNews(int newsId, String state, int adminId);
}
