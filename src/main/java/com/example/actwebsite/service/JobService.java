package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputJob;
import com.example.actwebsite.model.dto.OutputJob;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;

public interface JobService {

  /** Lấy danh sách Tin tuyển dụng có giới hạn */
  List<OutputJob> findLimit(int limit);

  /** Lấy danh sách tất cả Tin tuyển dụng */
  List<OutputJob> findAll();

  /** Lấy danh sách tất cả Tin tuyển dụng có phân trang và tìm kiếm theo yêu cầu */
  Page<OutputJob> getListJobPageable(SearchRequest search, int page, int size);

  /** Đếm số lượng Tin tuyển dụng theo phân loại (Lập trình, Mạng) */
  Map<String, Long> countNumberJobByType(StatisticTimeRequest statisticTimeRequest);

  /** Tìm tin tuyển dụng theo ID */
  OutputJob findById(int jobId);

  /** Tạo tin tuyển dụng */
  void createJob(InputJob data);

  /** Cập nhật tin tuyển dụng */
  boolean updateJob(int jobId, InputJob data);

  /** Xóa tin tuyển dụng */
  boolean deleteJob(int jobId, String reason);

  /** Lấy lại mật khẩu để chỉnh sửa Tin tuyển dụng */
  boolean forgetPassword(int jobId);

  /** Đăng nhập để chỉnh sửa Tin tuyển dụng */
  boolean loginJob(int jobId, String password);
}
