package com.example.actwebsite.service.impl;

import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.EmailService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.mail.internet.MimeMessage;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

  private final AdminRepository adminRepository;
  private final JavaMailSender javaMailSender;

  @Autowired
  public EmailServiceImpl(AdminRepository adminRepository, JavaMailSender javaMailSender) {
    this.adminRepository = adminRepository;
    this.javaMailSender = javaMailSender;
  }

  private Admin getManager() {
    return adminRepository
        .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
        .orElse(null);
  }

  @SneakyThrows
  @Override
  public void sendEmailHaveNewStudent(String name, String phone, String email, String courseName) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    String htmlMsg =
        "<!DOCTYPE html>\n"
            + "<html lang=\"en\">\n"
            + "<head>\n"
            + "  <meta charset=\"UTF-8\">\n"
            + "  <title>Title</title>\n"
            + "</head>\n"
            + "<body>\n"
            + "  <div>\n"
            + "    <p>Kính gửi quản lý "
            + getManager().getName()
            + " - Quản lý tuyển sinh học viện CNTT ACT</p>\n"
            + "    <p>Hôm nay đã có 1 bạn học viên đăng kí với thông tin như sau:</p>\n"
            + "    <p>- Họ và tên: "
            + name
            + "</p>\n"
            + "    <p>- Số điện thoại: "
            + phone
            + "</p>\n"
            + "    <p>- Email: "
            + email
            + "</p>\n"
            + "    <p>- Khóa học đăng kí: "
            + courseName
            + "</p>\n"
            + "    <p>Em xin gửi anh/chị thông tin của bạn !</p>\n"
            + "  </div>\n"
            + "</body>\n"
            + "</html>";
    message.setContent(htmlMsg, "text/html; charset=UTF-8");
    helper.setTo(getManager().getEmail());
    helper.setSubject("THÔNG TIN HỌC VIÊN MỚI NGÀY " + formatter.format(LocalDate.now()));
    this.javaMailSender.send(message);
  }

  @SneakyThrows
  @Override
  public void sendEmailCollaboratorForgetPassword(String email, String password) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    String htmlMsg =
        "<!DOCTYPE html>\n"
            + "<html lang=\"en\">\n"
            + "<head>\n"
            + "<meta charset=\"UTF-8\">\n"
            + "<title>Title</title>\n"
            + "</head>\n"
            + "<body>\n"
            + "<div>\n"
            + "<p>Kính gửi quản lý "
            + getManager().getName()
            + " - Quản lý tuyển sinh học viện CNTT ACT</p>\n"
            + "<p>Hôm nay tài khoản "
            + email
            + " có yêu cầu được lấy lại mật khẩu</p>\n"
            + "<p>Nếu anh/chị đồng ý thì xin hãy gửi mật khẩu mới này cho bạn: "
            + password
            + "</p>\n"
            + "<p>Em xin cảm ơn !</p>\n"
            + "</div>\n"
            + "</body>\n"
            + "</html>";
    message.setContent(htmlMsg, "text/html; charset=UTF-8");
    helper.setTo(getManager().getEmail());
    helper.setSubject(
        "YÊU CẦU LẤY LẠI MẬT KHẨU TÀI KHOẢN "
            + email
            + " NGÀY "
            + formatter.format(LocalDate.now()));
    this.javaMailSender.send(message);
  }

  @Override
  @SneakyThrows
  public void sendEmailNewAdmin(String name, String email) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    String htmlMsg =
        "<!DOCTYPE html>\n"
            + "<html lang=\"en\">\n"
            + "<head>\n"
            + "<meta charset=\"UTF-8\">\n"
            + "<title>Title</title>\n"
            + "</head>\n"
            + "<body>\n"
            + "<div>\n"
            + "<p>Kính gửi bạn "
            + name
            + " - Cộng tác viên của Học viện Công nghệ Thông tin ACT</p>\n"
            + "<p>Hôm nay ngày "
            + formatter.format(LocalDate.now())
            + ", bạn đã được bổ nhiệm chính thức thành Quản lý tuyển sinh mới của Học viện</p>\n"
            + "<p>Thay mặt phía Học viện, xin chúc mừng bạn với chức vụ mới </p>\n"
            + "<p>Rất mong bạn sẽ cố gắng hoàn thành tốt nhiệm vụ và nỗ lực hơn nữa trong thời gian sắp tới !</p>\n"
            + "</div>\n"
            + "</body>\n"
            + "</html>";
    message.setContent(htmlMsg, "text/html; charset=UTF-8");
    helper.setTo(email);
    helper.setSubject(
        "THÔNG BÁO THAY ĐỔI QUẢN LÝ HỌC VIỆN NGÀY " + formatter.format(LocalDate.now()));
    this.javaMailSender.send(message);
  }

  @Override
  @SneakyThrows
  public void sendEmailPosterNewPassword(String email, String password) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    String htmlMsg =
        "<!DOCTYPE html>\n"
            + "<html lang=\"en\">\n"
            + "<head>\n"
            + "<meta charset=\"UTF-8\">\n"
            + "<title>Title</title>\n"
            + "</head>\n"
            + "<body>\n"
            + "<div>\n"
            + "<p>Kính gửi chủ tài khoản email "
            + email
            + "</p>\n"
            + "<p>Mật khẩu mới dùng để chỉnh sửa tin tuyển dụng đã đăng là: <span style=\"color: red; font-weight: bold\">"
            + password
            + "</span></p>\n"
            + "<p>Thay mặt phía Học viện Công nghệ thông tin ACT, xin cảm ơn quý công ty đã tin tưởng sử dụng website của Học viện !</p>\n"
            + "</div>\n"
            + "</body>\n"
            + "</html>";
    message.setContent(htmlMsg, "text/html; charset=UTF-8");
    helper.setTo(email);
    helper.setSubject(
        "YÊU CẦU LẤY LẠI MẬT KHẨU CHỈNH SỬA TIN TUYỂN DỤNG NGÀY "
            + formatter.format(LocalDate.now()));
    this.javaMailSender.send(message);
  }

  @Override
  @SneakyThrows
  public void sendEmailPosterDeleteJob(String email, String jobName, String reason) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
    String htmlMsg =
        "<!DOCTYPE html>\n"
            + "<html lang=\"en\">\n"
            + "<head>\n"
            + "<meta charset=\"UTF-8\">\n"
            + "<title>Title</title>\n"
            + "</head>\n"
            + "<body>\n"
            + "<div>\n"
            + "<p>Kính gửi chủ tài khoản email "
            + email
            + "</p>\n"
            + "<p>Tin tuyển dụng <span style=\"color: red; font-weight: bold\">"
            + jobName
            + "</span> đã bị xóa khỏi hệ thống của Học viện</p>\n"
            + "<p>Với lý do: "
            + reason
            + "</p>\n"
            + "<p>Nếu đây là một sự nhầm lẫn, xin hãy phản hồi lại với Quản trị viên</p>\n"
            + "<p>Thay mặt phía Học viện Công nghệ thông tin ACT, xin cảm ơn quý công ty đã tin tưởng sử dụng website của Học viện !</p>\n"
            + "</div>\n"
            + "</body>\n"
            + "</html>";
    message.setContent(htmlMsg, "text/html; charset=UTF-8");
    helper.setTo(email);
    helper.setSubject(
        "THÔNG BÁO TIN TUYỂN DỤNG CỦA BẠN ĐÃ BỊ XÓA NGÀY " + formatter.format(LocalDate.now()));
    this.javaMailSender.send(message);
  }
}
