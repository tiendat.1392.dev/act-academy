package com.example.actwebsite.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.example.actwebsite.common.enums.ImgConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.entity.Course;
import com.example.actwebsite.model.dto.InputCourse;
import com.example.actwebsite.model.dto.OutputCourse;
import com.example.actwebsite.model.projection.CourseStatisticProjection;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.repository.ClassRepository;
import com.example.actwebsite.repository.CourseRepository;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.CourseService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseServiceImpl implements CourseService {

  private final CourseRepository courseRepository;
  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;
  private final ClassService classService;
  private final Cloudinary cloudinary;

  @Autowired
  public CourseServiceImpl(
      CourseRepository courseRepository,
      ClassRepository classRepository,
      AdminRepository adminRepository,
      GenericMapper genericMapper,
      ClassService classService,
      Cloudinary cloudinary) {
    this.courseRepository = courseRepository;
    this.adminRepository = adminRepository;
    this.genericMapper = genericMapper;
    this.classService = classService;
    this.cloudinary = cloudinary;
  }

  @Override
  public List<OutputCourse> findAll() {
    List<Course> listCourses =
        courseRepository.findAllByStateNot(
            StateConstant.DELETED.name(), Sort.by(Direction.DESC, "createdDate"));
    return genericMapper.mapToListOfType(listCourses, OutputCourse.class);
  }

  @Override
  public Page<OutputCourse> getListCoursePageable(SearchRequest search, int page, int size) {
    Page<Course> listCourses =
        courseRepository.findAllByNameAndTypeAndState(
            Utils.formatParameterQueryLike(search.getContent()),
            Utils.formatParameterQueryEqual(search.getType()),
            StateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listCourses,
        OutputCourse.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public Map<String, Long> countNumberCourseByType(StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }

    List<CourseStatisticProjection> courseStatisticProjections =
        courseRepository.countNumberCourseByType(dateStart, dateEnd, StateConstant.DELETED.name());
    Map<String, Long> result = new HashMap<>();
    result.put(TypeConstant.Programming.name(), 0L);
    result.put(TypeConstant.Network.name(), 0L);
    courseStatisticProjections.forEach(i -> result.put(i.getType(), i.getNumberCourse()));
    result.put("totalCourse", result.values().stream().reduce(0L, Long::sum));
    return result;
  }

  @Override
  public OutputCourse findById(int id) {
    Course course =
        courseRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    return genericMapper.mapToType(course, OutputCourse.class);
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean createCourse(InputCourse data, int adminId) {
    Course course =
        courseRepository
            .findByNameAndStateNot(data.getName(), StateConstant.DELETED.name())
            .orElse(null);
    if (course == null) {
      course = genericMapper.mapToType(data, Course.class);
      if (data.getFile() == null || data.getFile().isEmpty()) {
        course.setImg(ImgConstant.UnknownCourse.getValue());
      } else {
        String name = Utils.formatFileName(course.getName()) + "_" + System.currentTimeMillis();
        Transformation incoming = new Transformation().crop("fill").width(160).height(150);
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "course/" + name,
                    "transformation",
                    incoming));
        course.setImg(ImgConstant.Prefix.getValue() + "course/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      course.setState(StateConstant.ACTIVE.name());
      course.setCreatedBy(admin != null ? admin.getName() : "unknown");
      course.setCreatedDate(LocalDate.now());
      course.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      course.setUpdatedDate(LocalDate.now());
      Course result = courseRepository.save(course);
      // Tạo lớp dự bị cho khóa học
      classService.createPrepClass(result, admin);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean deleteCourse(int id, int adminId) {
    Course course =
        courseRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    if (course != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      course.setState(StateConstant.DELETED.name());
      course.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      course.setUpdatedDate(LocalDate.now());
      classService.deleteClass(classService.findPrepClassByCourseId(id).getId(), adminId);
      courseRepository.save(course);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean updateCourse(int courseId, InputCourse data, int adminId) {
    Course courseFindById =
        courseRepository.findByIdAndStateNot(courseId, StateConstant.DELETED.name()).orElse(null);
    Course courseFindByName =
        courseRepository
            .findByIdNotAndNameAndStateNot(courseId, data.getName(), StateConstant.DELETED.name())
            .orElse(null);
    if (courseFindById != null && courseFindByName == null) {
      genericMapper.copyNonNullProperties(data, courseFindById);
      if (data.getFile() != null && !data.getFile().isEmpty()) {
        String name = Utils.formatFileName(data.getName()) + "_" + System.currentTimeMillis();
        Transformation incoming = new Transformation().crop("fill").width(160).height(150);
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "course/" + name,
                    "transformation",
                    incoming));
        courseFindById.setImg(ImgConstant.Prefix.getValue() + "course/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      courseFindById.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      courseFindById.setUpdatedDate(LocalDate.now());
      courseRepository.save(courseFindById);
      return true;
    }
    return false;
  }
}
