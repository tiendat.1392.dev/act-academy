package com.example.actwebsite.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.example.actwebsite.common.enums.ImgConstant;
import com.example.actwebsite.common.enums.NewsStateConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.entity.News;
import com.example.actwebsite.model.dto.InputNews;
import com.example.actwebsite.model.dto.OutputNews;
import com.example.actwebsite.model.projection.NewsStatisticProjection;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.repository.NewsRepository;
import com.example.actwebsite.service.NewsService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewsServiceImpl implements NewsService {

  private final AdminRepository adminRepository;
  private final NewsRepository newsRepository;
  private final GenericMapper genericMapper;
  private final Cloudinary cloudinary;

  @Autowired
  public NewsServiceImpl(
      NewsRepository newsRepository,
      AdminRepository adminRepository,
      GenericMapper genericMapper,
      Cloudinary cloudinary) {
    this.newsRepository = newsRepository;
    this.adminRepository = adminRepository;
    this.genericMapper = genericMapper;
    this.cloudinary = cloudinary;
  }

  @Override
  public OutputNews findById(int id) {
    News news =
        newsRepository.findByIdAndStateNot(id, NewsStateConstant.DELETED.name()).orElse(null);
    return genericMapper.mapToType(news, OutputNews.class);
  }

  @Override
  public List<OutputNews> findLimit(int limit) {
    Page<News> listNews =
        newsRepository.findAllByTypeNotAndState(
            TypeConstant.Introduce.name(),
            NewsStateConstant.ACTIVE.name(),
            PageRequest.of(0, limit, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.mapToListOfType(listNews.getContent(), OutputNews.class);
  }

  @Override
  public List<OutputNews> findAll() {
    List<News> listNews =
        newsRepository.findAllByStateNot(
            NewsStateConstant.DELETED.name(), Sort.by(Direction.DESC, "createdDate"));
    return genericMapper.mapToListOfType(listNews, OutputNews.class);
  }

  @Override
  public List<OutputNews> findAllNewsIntroduce() {
    List<News> listNews =
        newsRepository.findAllByTypeAndStateNot(
            TypeConstant.Introduce.name(), NewsStateConstant.DELETED.name());
    return genericMapper.mapToListOfType(listNews, OutputNews.class);
  }

  @Override
  public Page<OutputNews> getListNewsPageable(
      SearchRequest search, String typeNot, int page, int size) {
    Page<News> listNews =
        newsRepository.findAllByNameOrSourceAndTypeAndState(
            Utils.formatParameterQueryLike(search.getContent()),
            Utils.formatParameterQueryEqual(search.getType()),
            Utils.formatParameterQueryEqual(search.getState()),
            Utils.formatParameterQueryEqual(typeNot),
            NewsStateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listNews,
        OutputNews.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public Map<String, Long> countNumberNewsByStateAndType(
      StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }
    List<NewsStatisticProjection> newsStatisticProjections =
        newsRepository.countNumberNewsByStateAndType(
            dateStart, dateEnd, NewsStateConstant.DELETED.name());
    Map<String, Long> result = new HashMap<>();
    result.put(NewsStateConstant.WAIT.name() + TypeConstant.Academy.name(), 0L);
    result.put(NewsStateConstant.WAIT.name() + TypeConstant.Tech.name(), 0L);
    result.put(NewsStateConstant.WAIT.name() + TypeConstant.Introduce.name(), 0L);
    result.put(NewsStateConstant.ACTIVE.name() + TypeConstant.Academy.name(), 0L);
    result.put(NewsStateConstant.ACTIVE.name() + TypeConstant.Tech.name(), 0L);
    result.put(NewsStateConstant.ACTIVE.name() + TypeConstant.Introduce.name(), 0L);
    newsStatisticProjections.forEach(
        i -> result.put(i.getState() + i.getType(), i.getNumberNews()));
    result.put("totalNews", result.values().stream().reduce(0L, Long::sum));
    result.put(
        "totalNewsWait",
        result.get(NewsStateConstant.WAIT.name() + TypeConstant.Academy.name())
            + result.get(NewsStateConstant.WAIT.name() + TypeConstant.Tech.name())
            + result.get(NewsStateConstant.WAIT.name() + TypeConstant.Introduce.name()));
    result.put(
        "totalNewsActive",
        result.get(NewsStateConstant.ACTIVE.name() + TypeConstant.Academy.name())
            + result.get(NewsStateConstant.ACTIVE.name() + TypeConstant.Tech.name())
            + result.get(NewsStateConstant.ACTIVE.name() + TypeConstant.Introduce.name()));
    return result;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean createNews(InputNews data, int adminId) {
    News news =
        newsRepository
            .findByNameAndStateNot(data.getName(), NewsStateConstant.DELETED.name())
            .orElse(null);
    if (news == null) {
      news = genericMapper.mapToType(data, News.class);
      if (data.getFile() == null || data.getFile().isEmpty()) {
        news.setImg(ImgConstant.UnknownNews.getValue());
      } else {
        String name = Utils.formatFileName(data.getName()) + "_" + System.currentTimeMillis();
        Transformation incoming = new Transformation().crop("fill").width(260).height(162);
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "news/" + name,
                    "transformation",
                    incoming));
        news.setImg(ImgConstant.Prefix.getValue() + "news/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      news.setPoster(admin);
      news.setState(NewsStateConstant.WAIT.name());
      news.setCreatedBy(admin != null ? admin.getName() : "unknown");
      news.setCreatedDate(LocalDate.now());
      news.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      news.setUpdatedDate(LocalDate.now());
      newsRepository.save(news);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean updateNews(int newsId, InputNews data, int adminId) {
    News newsById =
        newsRepository.findByIdAndStateNot(newsId, NewsStateConstant.DELETED.name()).orElse(null);
    News newsByName =
        newsRepository
            .findByIdNotAndNameAndStateNot(newsId, data.getName(), NewsStateConstant.DELETED.name())
            .orElse(null);
    if (newsById != null && newsByName == null) {
      genericMapper.copyNonNullProperties(data, newsById);
      if (data.getFile() != null && !data.getFile().isEmpty()) {
        String name = Utils.formatFileName(data.getName()) + "_" + System.currentTimeMillis();
        Transformation incoming = new Transformation().crop("fill").width(260).height(162);
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "news/" + name,
                    "transformation",
                    incoming));
        newsById.setImg(ImgConstant.Prefix.getValue() + "news/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      newsById.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      newsById.setUpdatedDate(LocalDate.now());
      newsRepository.save(newsById);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean changeStateNews(int newsId, String state, int adminId) {
    News news =
        newsRepository.findByIdAndStateNot(newsId, NewsStateConstant.DELETED.name()).orElse(null);
    if (news != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      news.setState(state);
      news.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      news.setUpdatedDate(LocalDate.now());
      newsRepository.save(news);
      return true;
    }
    return false;
  }
}
