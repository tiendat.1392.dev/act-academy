package com.example.actwebsite.service.impl;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.entity.Class;
import com.example.actwebsite.entity.Course;
import com.example.actwebsite.model.dto.InputClass;
import com.example.actwebsite.model.dto.OutputClass;
import com.example.actwebsite.model.projection.ClassStatisticProjection;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.repository.ClassRepository;
import com.example.actwebsite.service.ClassService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClassServiceImpl implements ClassService {

  private final ClassRepository classRepository;
  private final AdminRepository adminRepository;
  private final GenericMapper genericMapper;

  @Autowired
  public ClassServiceImpl(
      ClassRepository classRepository,
      AdminRepository adminRepository,
      GenericMapper genericMapper) {
    this.classRepository = classRepository;
    this.adminRepository = adminRepository;
    this.genericMapper = genericMapper;
  }

  @Override
  public List<OutputClass> findAll() {
    List<Class> listClasses =
        classRepository.findAllByStateNot(
            ClassStateConstant.DELETED.name(), Sort.by(Direction.DESC, "createdDate"));
    return genericMapper.mapToListOfType(listClasses, OutputClass.class);
  }

  @Override
  public Page<OutputClass> getListClassPageable(SearchRequest search, int page, int size) {
    Page<Class> listClasses =
        classRepository.findAllByNameOrTeacherNameOrSupporterNameOrManagerNameAndTypeAndState(
            Utils.formatParameterQueryLike(search.getContent()),
            Utils.formatParameterQueryEqual(search.getState()),
            ClassStateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listClasses,
        OutputClass.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public Page<OutputClass> getListClassPageableByCollaboratorId(
      int collaboratorId, int page, int size) {
    Page<Class> listClasses =
        classRepository.findAllByCollaboratorIdAndStateNot(
            collaboratorId,
            ClassStateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listClasses,
        OutputClass.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public Map<String, Long> countNumberClassByState(StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }
    List<ClassStatisticProjection> classStatisticProjections =
        classRepository.countNumberClassByState(
            dateStart, dateEnd, ClassStateConstant.DELETED.name());
    Map<String, Long> result = new HashMap<>();
    result.put(ClassStateConstant.PREP.name(), 0L);
    result.put(ClassStateConstant.WAIT.name(), 0L);
    result.put(ClassStateConstant.DOING.name(), 0L);
    result.put(ClassStateConstant.FINISHED.name(), 0L);
    classStatisticProjections.forEach(i -> result.put(i.getState(), i.getNumberClass()));
    result.put("totalClass", result.values().stream().reduce(0L, Long::sum));
    return result;
  }

  @Override
  public OutputClass findById(int id) {
    Class result =
        classRepository.findByIdAndStateNot(id, ClassStateConstant.DELETED.name()).orElse(null);
    return genericMapper.mapToType(result, OutputClass.class);
  }

  @Override
  public OutputClass findPrepClassByCourseId(int courseId) {
    Class result =
        classRepository
            .findByCourseIdAndState(courseId, ClassStateConstant.PREP.name())
            .orElse(null);
    return genericMapper.mapToType(result, OutputClass.class);
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean createClass(InputClass data, int adminId) {
    Class entity =
        classRepository
            .findClassByIdNotAndNameOrFullNameAndStateNot(
                0, data.getName(), data.getFullName(), StateConstant.DELETED.name())
            .orElse(null);
    if (entity == null) {
      entity = genericMapper.mapToType(data, Class.class);
      Admin admin = adminRepository.findById(adminId).orElse(null);
      entity.setNumberStudent(0);
      entity.setState(ClassStateConstant.WAIT.name());
      entity.setCreatedBy(admin != null ? admin.getName() : "unknown");
      entity.setCreatedDate(LocalDate.now());
      entity.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      entity.setUpdatedDate(LocalDate.now());
      classRepository.save(entity);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public void createPrepClass(Course course, Admin admin) {
    Class prepClass = new Class();
    prepClass.setFullName(course.getName());
    prepClass.setName(course.getName());
    prepClass.setNumberStudent(0);
    prepClass.setHowToLearn("Offline");
    prepClass.setDateOpening(LocalDate.now());
    prepClass.setTeacherName("Hoàng Thanh Nam");
    prepClass.setManager(admin);
    prepClass.setSupporter(admin);
    prepClass.setCourse(course);
    prepClass.setState(ClassStateConstant.PREP.name());
    prepClass.setCreatedBy(admin.getName());
    prepClass.setCreatedDate(LocalDate.now());
    prepClass.setUpdatedBy(admin.getName());
    prepClass.setUpdatedDate(LocalDate.now());
    classRepository.save(prepClass);
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean updateClass(int classId, InputClass data, int adminId) {
    Class classFindById =
        classRepository
            .findByIdAndStateNot(classId, ClassStateConstant.DELETED.name())
            .orElse(null);
    Class classCourse =
        classRepository
            .findClassByIdNotAndNameOrFullNameAndStateNot(
                classId, data.getName(), data.getFullName(), StateConstant.DELETED.name())
            .orElse(null);
    if (classFindById != null && classCourse == null) {
      genericMapper.copyNonNullProperties(data, classFindById);
      Admin admin = adminRepository.findById(adminId).orElse(null);
      classFindById.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      classFindById.setUpdatedDate(LocalDate.now());
      classRepository.save(classFindById);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public void updateNumberStudent(int id, int numberStudent, int adminId) {
    Class entity =
        classRepository.findByIdAndStateNot(id, ClassStateConstant.DELETED.name()).orElse(null);
    if (entity != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      entity.setNumberStudent(numberStudent);
      entity.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      entity.setUpdatedDate(LocalDate.now());
      classRepository.save(entity);
    }
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean deleteClass(int classId, int adminId) {
    Class entity =
        classRepository
            .findByIdAndStateNot(classId, ClassStateConstant.DELETED.name())
            .orElse(null);
    if (entity != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      entity.setState(ClassStateConstant.DELETED.name());
      entity.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      entity.setUpdatedDate(LocalDate.now());
      return true;
    }
    return false;
  }
}
