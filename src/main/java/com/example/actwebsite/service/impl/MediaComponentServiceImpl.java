package com.example.actwebsite.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.example.actwebsite.common.enums.ImgConstant;
import com.example.actwebsite.entity.MediaComponents;
import com.example.actwebsite.model.dto.InputMediaComponent;
import com.example.actwebsite.model.request.SliderUpdateRequest;
import com.example.actwebsite.repository.MediaComponentRepository;
import com.example.actwebsite.service.MediaComponentService;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MediaComponentServiceImpl implements MediaComponentService {

  private final MediaComponentRepository mediaComponentRepository;
  private final Cloudinary cloudinary;

  @Autowired
  public MediaComponentServiceImpl(
      MediaComponentRepository mediaComponentRepository, Cloudinary cloudinary) {
    this.mediaComponentRepository = mediaComponentRepository;
    this.cloudinary = cloudinary;
  }

  @Override
  public String getUrlVideoBanner() {
    MediaComponents media = mediaComponentRepository.findByName("video_banner").orElse(null);
    return Objects.requireNonNull(media).getUrl();
  }

  @Override
  public Map<String, String> getSlider() {
    List<MediaComponents> slider = mediaComponentRepository.findAllByNameNot("video_banner");
    Map<String, String> result = new TreeMap<>();
    slider.forEach(i -> result.put(i.getName(), i.getUrl()));
    return result;
  }

  @Override
  @SneakyThrows
  public void updateVideoBanner(InputMediaComponent data) {
    MediaComponents video = mediaComponentRepository.findByName("video_banner").orElse(null);
    if (video != null && data.getFile() != null) {
      String name = "video_banner_" + System.currentTimeMillis();
      Transformation incoming = new Transformation().width(1350).height(185).crop("fill").chain();
      this.cloudinary
          .uploader()
          .upload(
              data.getFile().getBytes(),
              ObjectUtils.asMap(
                  "resource_type",
                  "auto",
                  "public_id",
                  "media/" + name,
                  "transformation",
                  incoming));
      video.setUrl("https://res.cloudinary.com/dralhpdiv/video/upload/v1649758412/media/" + name);
      mediaComponentRepository.save(video);
    }
  }

  @Override
  @SneakyThrows
  public void updateSlider(SliderUpdateRequest data) {
    List<MediaComponents> slider = mediaComponentRepository.findAllByNameNot("video_banner");
    slider.forEach(
        i -> {
          if (data.getFile1() != null
              && !data.getFile1().isEmpty()
              && i.getName().equals("slider_01")) {
            String name = "slider_01_" + System.currentTimeMillis();
            uploadImageSlider(data.getFile1(), name);
            i.setUrl(ImgConstant.Prefix.getValue() + "media/" + name);
          }
          if (data.getFile2() != null
              && !data.getFile2().isEmpty()
              && i.getName().equals("slider_02")) {
            String name = "slider_02_" + System.currentTimeMillis();
            uploadImageSlider(data.getFile2(), name);
            i.setUrl(ImgConstant.Prefix.getValue() + "media/" + name);
          }
          if (data.getFile3() != null
              && !data.getFile3().isEmpty()
              && i.getName().equals("slider_03")) {
            String name = "slider_03_" + System.currentTimeMillis();
            uploadImageSlider(data.getFile3(), name);
            i.setUrl(ImgConstant.Prefix.getValue() + "media/" + name);
          }
          if (data.getFile4() != null
              && !data.getFile4().isEmpty()
              && i.getName().equals("slider_04")) {
            String name = "slider_04_" + System.currentTimeMillis();
            uploadImageSlider(data.getFile4(), name);
            i.setUrl(ImgConstant.Prefix.getValue() + "media/" + name);
          }
          if (data.getFile5() != null
              && !data.getFile5().isEmpty()
              && i.getName().equals("slider_05")) {
            String name = "slider_05_" + System.currentTimeMillis();
            uploadImageSlider(data.getFile1(), name);
            i.setUrl(ImgConstant.Prefix.getValue() + "media/" + name);
          }
        });
    mediaComponentRepository.saveAll(slider);
  }

  @SneakyThrows
  public void uploadImageSlider(MultipartFile file, String name) {
    Transformation incoming = new Transformation().width(1350).height(550).crop("scale").chain();
    this.cloudinary
        .uploader()
        .upload(
            file.getBytes(),
            ObjectUtils.asMap(
                "resource_type", "auto", "public_id", "media/" + name, "transformation", incoming));
  }
}
