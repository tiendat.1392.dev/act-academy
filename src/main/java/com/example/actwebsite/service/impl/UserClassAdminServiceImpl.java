package com.example.actwebsite.service.impl;

import com.example.actwebsite.common.enums.ClassStateConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.enums.StudentStateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.entity.Class;
import com.example.actwebsite.entity.Course;
import com.example.actwebsite.entity.User;
import com.example.actwebsite.entity.UserClassAdmin;
import com.example.actwebsite.model.dto.InputUser;
import com.example.actwebsite.model.dto.OutputStudentStatistic;
import com.example.actwebsite.model.dto.OutputUsersClassAdmin;
import com.example.actwebsite.model.projection.StudentStatisticProjection;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.repository.ClassRepository;
import com.example.actwebsite.repository.CourseRepository;
import com.example.actwebsite.repository.UserClassAdminRepository;
import com.example.actwebsite.repository.UserRepository;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.EmailService;
import com.example.actwebsite.service.UserClassAdminService;
import com.example.actwebsite.service.UserService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserClassAdminServiceImpl implements UserClassAdminService {

  private final UserClassAdminRepository userClassAdminRepository;
  private final CourseRepository courseRepository;
  private final AdminRepository adminRepository;
  private final ClassService classService;
  private final ClassRepository classRepository;
  private final UserService userService;
  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final EmailService emailService;

  @Autowired
  public UserClassAdminServiceImpl(
      UserClassAdminRepository userClassAdminRepository,
      AdminRepository adminRepository,
      CourseRepository courseRepository,
      ClassService classService,
      UserService userService,
      UserRepository userRepository,
      ClassRepository classRepository,
      GenericMapper genericMapper,
      EmailService emailService) {
    this.userClassAdminRepository = userClassAdminRepository;
    this.adminRepository = adminRepository;
    this.courseRepository = courseRepository;
    this.classService = classService;
    this.userService = userService;
    this.userRepository = userRepository;
    this.classRepository = classRepository;
    this.genericMapper = genericMapper;
    this.emailService = emailService;
  }

  @Override
  public Page<OutputUsersClassAdmin> findByClassId(
      int classId, SearchRequest search, int page, int size) {
    Page<UserClassAdmin> listUserClassAdmins =
        userClassAdminRepository
            .findAllByClassIdAndStudentNameOrStudentEmailOrStudentPhoneAndState(
                classId,
                Utils.formatParameterQueryLike(search.getContent()),
                Utils.formatParameterQueryEqual(search.getState()),
                StudentStateConstant.DELETED.name(),
                PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listUserClassAdmins,
        OutputUsersClassAdmin.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public Page<OutputUsersClassAdmin> findByStudentId(int studentId, int page, int size) {
    Page<UserClassAdmin> listUserClassAdmins =
        userClassAdminRepository.findAllByStudentIdAndStateNot(
            studentId,
            StudentStateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listUserClassAdmins,
        OutputUsersClassAdmin.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public List<OutputStudentStatistic> countNumberStudentByState(
      StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }
    List<StudentStatisticProjection> userStatisticProjections =
        userClassAdminRepository.countNumberStudentByState(
            dateStart, dateEnd, StudentStateConstant.DELETED.name());
    List<OutputStudentStatistic> result = new ArrayList<>();
    if (userStatisticProjections != null && !userStatisticProjections.isEmpty()) {
      OutputStudentStatistic statistic = new OutputStudentStatistic();
      String className = userStatisticProjections.get(0).getClassCourse().getName();
      for (StudentStatisticProjection index : userStatisticProjections) {
        if (!index.getClassCourse().getName().equals(className)) {
          statistic.setClassName(className);
          statistic.setTotalStudent(
              statistic.getNumberNewStudent()
                  + statistic.getNumberWaitStudent()
                  + statistic.getNumberPayHalfStudent()
                  + statistic.getNumberConfirmStudent());
          result.add(statistic);
          statistic = new OutputStudentStatistic();
          className = index.getClassCourse().getName();
        }
        if (StudentStateConstant.NEW.name().equals(index.getState())) {
          statistic.setNumberNewStudent(index.getNumberStudent());
        } else if (StudentStateConstant.WAIT.name().equals(index.getState())) {
          statistic.setNumberWaitStudent(index.getNumberStudent());
        } else if (StudentStateConstant.PAY_HALF.name().equals(index.getState())) {
          statistic.setNumberPayHalfStudent(index.getNumberStudent());
        } else if (StudentStateConstant.CONFIRM.name().equals(index.getState())) {
          statistic.setNumberConfirmStudent(index.getNumberStudent());
        }
        if (index == userStatisticProjections.get(userStatisticProjections.size() - 1)) {
          statistic.setClassName(className);
          statistic.setTotalStudent(
              statistic.getNumberNewStudent()
                  + statistic.getNumberWaitStudent()
                  + statistic.getNumberPayHalfStudent()
                  + statistic.getNumberConfirmStudent());
          result.add(statistic);
        }
      }
    }
    return result;
  }

  @Override
  @SneakyThrows
  @Transactional
  public OutputUsersClassAdmin createStudentWithCourse(
      InputUser data, int courseId, int collaboratorId) {
    Course course =
        courseRepository.findByIdAndStateNot(courseId, StateConstant.DELETED.name()).orElse(null);
    if (course != null) {
      Admin collaborator =
          adminRepository
              .findByIdAndStateNot(collaboratorId, StateConstant.DELETED.name())
              .orElse(null);
      User student =
          userRepository
              .findByEmailAndStateNot(data.getEmail(), StateConstant.DELETED.name())
              .orElse(null);
      if (student == null) {
        // Tạo mới thông tin Học viên do Học viên chưa có trong hệ thống
        student = genericMapper.mapToType(userService.createUser(data, 0), User.class);
      }
      Class classCourse =
          classRepository
              .findByCourseIdAndState(course.getId(), ClassStateConstant.WAIT.name())
              .orElse(null);
      if (classCourse == null) {
        // Tìm lớp học dự bị của Khóa học do chưa có lớp sắp khai giảng ứng với Khóa học này
        classCourse =
            classRepository
                .findByCourseIdAndState(course.getId(), ClassStateConstant.PREP.name())
                .orElse(null);
      }
      UserClassAdmin userClassAdmin =
          userClassAdminRepository
              .findByClassCourseIdAndStudentIdAndStateNot(
                  Objects.requireNonNull(classCourse).getId(),
                  student.getId(),
                  StudentStateConstant.DELETED.name())
              .orElse(null);
      if (userClassAdmin == null) {
        userClassAdmin = new UserClassAdmin();
        userClassAdmin.setCollaborator(collaborator);
        userClassAdmin.setClassCourse(classCourse);
        userClassAdmin.setStudent(student);
        userClassAdmin.setState(StudentStateConstant.NEW.name());
        userClassAdmin.setCreatedBy(collaborator != null ? collaborator.getName() : "unknown");
        userClassAdmin.setCreatedDate(LocalDate.now());
        userClassAdmin.setUpdatedBy(collaborator != null ? collaborator.getName() : "unknown");
        userClassAdmin.setUpdatedDate(LocalDate.now());
        // Cập nhật số lượng học viên có trong Lớp học
        classService.updateNumberStudent(
            Objects.requireNonNull(classCourse).getId(), classCourse.getNumberStudent() + 1, 0);
        // Gửi email thông báo cho Quản lý có Học viên mới
        emailService.sendEmailHaveNewStudent(
            student.getName(), student.getPhone(), student.getEmail(), course.getName());
        return genericMapper.mapToType(
            userClassAdminRepository.save(userClassAdmin), OutputUsersClassAdmin.class);
      }
    }
    return null;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean createStudentWithClass(InputUser data, int classId, int collaboratorId) {
    Class classCourse =
        classRepository
            .findByIdAndStateNot(classId, ClassStateConstant.DELETED.name())
            .orElse(null);
    if (classCourse != null) {
      Admin collaborator =
          adminRepository
              .findByIdAndStateNot(collaboratorId, StateConstant.DELETED.name())
              .orElse(null);
      User student =
          userRepository
              .findByEmailAndStateNot(data.getEmail(), StateConstant.DELETED.name())
              .orElse(null);
      if (student == null) {
        student = genericMapper.mapToType(userService.createUser(data, collaboratorId), User.class);
      }
      UserClassAdmin userClassAdmin =
          userClassAdminRepository
              .findByClassCourseIdAndStudentIdAndStateNot(
                  classCourse.getId(), student.getId(), StudentStateConstant.DELETED.name())
              .orElse(null);
      if (userClassAdmin == null) {
        userClassAdmin = new UserClassAdmin();
        userClassAdmin.setCollaborator(collaborator);
        userClassAdmin.setClassCourse(classCourse);
        userClassAdmin.setStudent(student);
        userClassAdmin.setState(StudentStateConstant.NEW.name());
        userClassAdmin.setCreatedBy(collaborator != null ? collaborator.getName() : "unknown");
        userClassAdmin.setCreatedDate(LocalDate.now());
        userClassAdmin.setUpdatedBy(collaborator != null ? collaborator.getName() : "unknown");
        userClassAdmin.setUpdatedDate(LocalDate.now());
        userClassAdminRepository.save(userClassAdmin);
        // Cập nhật số lượng học viên có trong Lớp học
        classService.updateNumberStudent(
            classCourse.getId(), classCourse.getNumberStudent() + 1, collaboratorId);
        return true;
      }
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean addStudentToClassByEmail(int classId, String email, int adminId) {
    User student =
        userRepository.findByEmailAndStateNot(email, StateConstant.DELETED.name()).orElse(null);
    Class classCourse =
        classRepository
            .findByIdAndStateNot(classId, ClassStateConstant.DELETED.name())
            .orElse(null);
    if (student != null && classCourse != null) {
      UserClassAdmin userClassAdmin =
          userClassAdminRepository
              .findByClassCourseIdAndStudentIdAndStateNot(
                  classId, student.getId(), StudentStateConstant.DELETED.name())
              .orElse(null);
      if (userClassAdmin == null) {
        userClassAdmin = new UserClassAdmin();
        Admin admin = adminRepository.findById(adminId).orElse(null);
        userClassAdmin.setCollaborator(admin);
        userClassAdmin.setClassCourse(classCourse);
        userClassAdmin.setStudent(student);
        userClassAdmin.setState(StudentStateConstant.NEW.name());
        userClassAdmin.setCreatedBy(admin != null ? admin.getName() : "unknown");
        userClassAdmin.setCreatedDate(LocalDate.now());
        userClassAdmin.setUpdatedBy(admin != null ? admin.getName() : "unknown");
        userClassAdmin.setUpdatedDate(LocalDate.now());
        userClassAdminRepository.save(userClassAdmin);
        // Cập nhật số lượng học viên có trong Lớp học
        classService.updateNumberStudent(
            classCourse.getId(), classCourse.getNumberStudent() + 1, adminId);
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean changeStateStudentInClass(int classId, int studentId, String state, int adminId) {
    UserClassAdmin userClassAdmin =
        userClassAdminRepository
            .findByClassCourseIdAndStudentIdAndStateNot(
                classId, studentId, StudentStateConstant.DELETED.name())
            .orElse(null);
    if (userClassAdmin != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      userClassAdmin.setState(state);
      userClassAdmin.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      userClassAdmin.setUpdatedDate(LocalDate.now());
      userClassAdminRepository.save(userClassAdmin);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean deleteStudent(int classId, int studentId, int adminId) {
    UserClassAdmin userClassAdmin =
        userClassAdminRepository
            .findByClassCourseIdAndStudentIdAndStateNot(
                classId, studentId, StudentStateConstant.DELETED.name())
            .orElse(null);
    Class classCourse =
        classRepository
            .findByIdAndStateNot(classId, ClassStateConstant.DELETED.name())
            .orElse(null);
    if (userClassAdmin != null && classCourse != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      userClassAdmin.setState(StudentStateConstant.DELETED.name());
      userClassAdmin.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      userClassAdmin.setUpdatedDate(LocalDate.now());
      userClassAdminRepository.save(userClassAdmin);
      // Cập nhật số lượng học viên có trong Lớp học
      classService.updateNumberStudent(
          classCourse.getId(), classCourse.getNumberStudent() - 1, adminId);
      return true;
    }
    return false;
  }
}
