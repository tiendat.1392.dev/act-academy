package com.example.actwebsite.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.example.actwebsite.common.enums.ImgConstant;
import com.example.actwebsite.common.enums.RoleConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.model.dto.InputAdmin;
import com.example.actwebsite.model.dto.OutputAdmin;
import com.example.actwebsite.model.projection.AdminStatisticProjection;
import com.example.actwebsite.model.request.ChangePasswordRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.service.AdminService;
import com.example.actwebsite.service.EmailService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.SneakyThrows;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminServiceImpl implements AdminService {

  private final Cloudinary cloudinary;
  private final EmailService emailService;
  private final GenericMapper genericMapper;
  private final AdminRepository adminRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public AdminServiceImpl(
      Cloudinary cloudinary,
      EmailService emailService,
      GenericMapper genericMapper,
      AdminRepository adminRepository,
      PasswordEncoder passwordEncoder) {
    this.cloudinary = cloudinary;
    this.emailService = emailService;
    this.genericMapper = genericMapper;
    this.adminRepository = adminRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public List<OutputAdmin> findAll() {
    List<Admin> listAdmin =
        adminRepository.findAllByRoleNotAndStateNot(
            RoleConstant.Owner.name(),
            StateConstant.DELETED.name(),
            Sort.by(Direction.DESC, "createdDate"));
    return genericMapper.mapToListOfType(listAdmin, OutputAdmin.class);
  }

  @Override
  public Page<OutputAdmin> getListAdminPageable(SearchRequest search, int page, int size) {
    Page<Admin> listAdmins =
        adminRepository.findAllByNameOrEmailOrPhoneAndStateNot(
            Utils.formatParameterQueryLike(search.getContent()),
            RoleConstant.Collaborator.name(),
            StateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listAdmins,
        OutputAdmin.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public Map<String, Long> countNumberCollaboratorByRole(
      StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }
    List<AdminStatisticProjection> adminStatisticProjections =
        adminRepository.countNumberCollaboratorByRole(
            dateStart, dateEnd, StateConstant.DELETED.name());
    Map<String, Long> result = new HashMap<>();
    result.put(RoleConstant.Manager.name(), 0L);
    result.put(RoleConstant.Collaborator.name(), 0L);
    adminStatisticProjections.forEach(i -> result.put(i.getRole(), i.getNumberCollaborator()));
    result.put("totalCollaborator", result.values().stream().reduce(0L, Long::sum));
    return result;
  }

  @Override
  public OutputAdmin findById(int id) {
    Admin admin =
        adminRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    if (admin != null && EnumUtils.isValidEnum(RoleConstant.class, admin.getRole())) {
      admin.setRole(RoleConstant.valueOf(admin.getRole()).getValue());
    }
    return genericMapper.mapToType(admin, OutputAdmin.class);
  }

  @Override
  public OutputAdmin findByEmail(String email) {
    Admin admin =
        adminRepository.findByEmailAndStateNot(email, StateConstant.DELETED.name()).orElse(null);
    return genericMapper.mapToType(admin, OutputAdmin.class);
  }

  @Override
  public OutputAdmin findByRole(String role) {
    Admin admin =
        adminRepository.findByRoleAndStateNot(role, StateConstant.DELETED.name()).orElse(null);
    return genericMapper.mapToType(admin, OutputAdmin.class);
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean createAdmin(InputAdmin data, int adminId) {
    Admin entity =
        adminRepository
            .findByEmailAndStateNot(data.getEmail(), StateConstant.DELETED.name())
            .orElse(null);
    if (entity == null) {
      entity = genericMapper.mapToType(data, Admin.class);
      if (data.getFile() == null || data.getFile().isEmpty()) {
        entity.setImg(ImgConstant.UnknownUser.getValue());
      } else {
        String name = Utils.formatFileName(data.getEmail()) + "_" + System.currentTimeMillis();
        Transformation incoming =
            new Transformation()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "admin_user/" + name,
                    "transformation",
                    incoming));
        entity.setImg(ImgConstant.Prefix.getValue() + "admin_user/" + name);
      }
      if (Utils.stringIsNull(entity.getPassword())) {
        entity.setPassword("Abc@1234");
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      entity.setRole(RoleConstant.Collaborator.name());
      entity.setPassword(passwordEncoder.encode(data.getPassword()));
      entity.setState(StateConstant.ACTIVE.name());
      entity.setCreatedBy(admin != null ? admin.getName() : "unknown");
      entity.setCreatedDate(LocalDate.now());
      entity.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      entity.setUpdatedDate(LocalDate.now());
      adminRepository.save(entity);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean updateAdmin(int id, InputAdmin data, int adminId) {
    Admin adminById =
        adminRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    Admin adminByEmail =
        adminRepository
            .findByIdNotAndEmailAndStateNot(id, data.getEmail(), StateConstant.DELETED.name())
            .orElse(null);
    if (adminById != null && adminByEmail == null) {
      genericMapper.copyNonNullProperties(data, adminById);
      if (data.getFile() != null && !data.getFile().isEmpty()) {
        String name = Utils.formatFileName(data.getEmail()) + "_" + System.currentTimeMillis();
        Transformation incoming =
            new Transformation()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "admin_user/" + name,
                    "transformation",
                    incoming));
        adminById.setImg(ImgConstant.Prefix.getValue() + "admin_user/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      adminById.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      adminById.setUpdatedDate(LocalDate.now());
      adminRepository.save(adminById);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean changePassword(int id, ChangePasswordRequest data) {
    Admin admin =
        adminRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    if (admin != null) {
      if (passwordEncoder.matches(data.getOldPassword(), admin.getPassword())) {
        admin.setPassword(passwordEncoder.encode(data.getNewPassword()));
        admin.setUpdatedBy(admin.getName());
        admin.setUpdatedDate(LocalDate.now());
        adminRepository.save(admin);
        return true;
      }
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean deleteAdmin(int id, int adminId) {
    Admin entity =
        adminRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    if (entity != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      entity.setState(StateConstant.DELETED.name());
      entity.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      entity.setUpdatedDate(LocalDate.now());
      adminRepository.save(entity);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean forgetPassword(String email) {
    Admin admin =
        adminRepository.findByEmailAndStateNot(email, StateConstant.DELETED.name()).orElse(null);
    if (admin != null) {
      String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
      String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
      String numbers = RandomStringUtils.randomNumeric(2);
      String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
      String password =
          upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(specialChar);
      admin.setPassword(passwordEncoder.encode(password));
      adminRepository.save(admin);
      // Gửi mật khẩu mới về email Quản lý để Quản lý đưa cho bạn Cộng tác viên
      emailService.sendEmailCollaboratorForgetPassword(email, password);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean changeAdmin(String email, int adminId) {
    Admin newAdmin =
        adminRepository.findByEmailAndStateNot(email, StateConstant.DELETED.name()).orElse(null);
    if (newAdmin != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      Admin oldAdmin =
          adminRepository
              .findByRoleAndStateNot(RoleConstant.Manager.name(), StateConstant.DELETED.name())
              .orElse(null);
      Objects.requireNonNull(oldAdmin).setRole(RoleConstant.Collaborator.name());
      oldAdmin.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      oldAdmin.setUpdatedDate(LocalDate.now());
      adminRepository.save(oldAdmin);
      newAdmin.setRole(RoleConstant.Manager.name());
      newAdmin.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      newAdmin.setUpdatedDate(LocalDate.now());
      adminRepository.save(newAdmin);
      emailService.sendEmailNewAdmin(newAdmin.getName(), newAdmin.getEmail());
      return true;
    }
    return false;
  }
}
