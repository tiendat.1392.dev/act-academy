package com.example.actwebsite.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.example.actwebsite.common.enums.ImgConstant;
import com.example.actwebsite.common.enums.TypeConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Job;
import com.example.actwebsite.model.dto.InputJob;
import com.example.actwebsite.model.dto.OutputJob;
import com.example.actwebsite.model.projection.JobStatisticProjection;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.JobRepository;
import com.example.actwebsite.service.EmailService;
import com.example.actwebsite.service.JobService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JobServiceImpl implements JobService {

  private final Cloudinary cloudinary;
  private final EmailService emailService;
  private final JobRepository jobRepository;
  private final GenericMapper genericMapper;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public JobServiceImpl(
      Cloudinary cloudinary,
      EmailService emailService,
      JobRepository jobRepository,
      GenericMapper genericMapper,
      PasswordEncoder passwordEncoder) {
    this.cloudinary = cloudinary;
    this.emailService = emailService;
    this.jobRepository = jobRepository;
    this.genericMapper = genericMapper;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public List<OutputJob> findLimit(int limit) {
    Page<Job> listJobs =
        jobRepository.findAll(PageRequest.of(0, limit, Sort.by(Direction.DESC, "datePosted")));
    return genericMapper.mapToListOfType(listJobs.getContent(), OutputJob.class);
  }

  @Override
  public List<OutputJob> findAll() {
    List<Job> listJobs = jobRepository.findAll(Sort.by(Direction.DESC, "datePosted"));
    return genericMapper.mapToListOfType(listJobs, OutputJob.class);
  }

  @Override
  public Page<OutputJob> getListJobPageable(SearchRequest search, int page, int size) {
    Page<Job> listJobs =
        jobRepository.findAllByNameOrContactOrPhoneOrEmailAndType(
            Utils.formatParameterQueryLike(search.getContent()),
            Utils.formatParameterQueryEqual(search.getType()),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "datePosted")));
    return genericMapper.toPage(
        listJobs,
        OutputJob.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "datePosted")));
  }

  @Override
  public Map<String, Long> countNumberJobByType(StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }
    List<JobStatisticProjection> jobStatisticProjections =
        jobRepository.countNumberJobByType(dateStart, dateEnd);
    Map<String, Long> result = new HashMap<>();
    result.put(TypeConstant.Programming.name(), 0L);
    result.put(TypeConstant.Network.name(), 0L);
    jobStatisticProjections.forEach(i -> result.put(i.getType(), i.getNumberJob()));
    result.put("totalJob", result.values().stream().reduce(0L, Long::sum));
    return result;
  }

  @Override
  public OutputJob findById(int id) {
    Job job = jobRepository.findById(id).orElse(null);
    return genericMapper.mapToType(job, OutputJob.class);
  }

  @Override
  @SneakyThrows
  @Transactional
  public void createJob(InputJob data) {
    Job job = genericMapper.mapToType(data, Job.class);
    if (data.getFile() != null && !data.getFile().isEmpty()) {
      String name = Utils.formatFileName(job.getName()) + "_" + System.currentTimeMillis();
      Transformation incoming = new Transformation().crop("fill").width(264).height(149);
      this.cloudinary
          .uploader()
          .upload(
              data.getFile().getBytes(),
              ObjectUtils.asMap(
                  "resource_type", "auto", "public_id", "job/" + name, "transformation", incoming));
      job.setImg(ImgConstant.Prefix.getValue() + "job/" + name);
    } else {
      job.setImg(ImgConstant.UnknownJob.getValue());
    }
    job.setPassword(passwordEncoder.encode(data.getPassword()));
    job.setDatePosted(LocalDate.now());
    jobRepository.save(job);
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean updateJob(int jobId, InputJob data) {
    Job job = jobRepository.findById(jobId).orElse(null);
    if (job != null) {
      genericMapper.copyNonNullProperties(data, job);
      if (data.getFile() != null && !data.getFile().isEmpty()) {
        String name = Utils.formatFileName(job.getName()) + "_" + System.currentTimeMillis();
        Transformation incoming = new Transformation().crop("fill").width(264).height(149);
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "job/" + name,
                    "transformation",
                    incoming));
        job.setImg(ImgConstant.Prefix.getValue() + "job/" + name);
      }
      jobRepository.save(job);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean deleteJob(int jobId, String reason) {
    Job job = jobRepository.findById(jobId).orElse(null);
    if (job != null) {
      if (!Utils.stringIsNull(reason)) {
        // Gửi email kèm lý do Tin tuyển dụng bị xóa cho Người đăng
        emailService.sendEmailPosterDeleteJob(job.getEmail(), job.getName(), reason);
      }
      jobRepository.delete(job);
      return true;
    }
    return false;
  }

  @Override
  public boolean forgetPassword(int jobId) {
    Job job = jobRepository.findById(jobId).orElse(null);
    if (job != null) {
      String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
      String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
      String numbers = RandomStringUtils.randomNumeric(2);
      String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
      String password =
          upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(specialChar);
      job.setPassword(passwordEncoder.encode(password));
      jobRepository.save(job);
      // Gửi email kèm mật khẩu mới dùng để chỉnh sửa tin tuyển dụng cho Người đăng
      emailService.sendEmailPosterNewPassword(job.getEmail(), password);
      return true;
    }
    return false;
  }

  @Override
  public boolean loginJob(int jobId, String password) {
    Job job = jobRepository.findById(jobId).orElse(null);
    return job != null && passwordEncoder.matches(password, job.getPassword());
  }
}
