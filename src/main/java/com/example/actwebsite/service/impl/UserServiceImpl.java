package com.example.actwebsite.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.example.actwebsite.common.enums.ImgConstant;
import com.example.actwebsite.common.enums.StateConstant;
import com.example.actwebsite.common.enums.StudentStateConstant;
import com.example.actwebsite.common.util.Utils;
import com.example.actwebsite.component.GenericMapper;
import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.entity.User;
import com.example.actwebsite.entity.UserClassAdmin;
import com.example.actwebsite.model.dto.InputUser;
import com.example.actwebsite.model.dto.OutputUser;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import com.example.actwebsite.repository.AdminRepository;
import com.example.actwebsite.repository.UserClassAdminRepository;
import com.example.actwebsite.repository.UserRepository;
import com.example.actwebsite.service.ClassService;
import com.example.actwebsite.service.UserService;
import java.time.LocalDate;
import java.util.List;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

  private final UserClassAdminRepository userClassAdminRepository;
  private final AdminRepository adminRepository;
  private final UserRepository userRepository;
  private final GenericMapper genericMapper;
  private final ClassService classService;
  private final Cloudinary cloudinary;

  @Autowired
  public UserServiceImpl(
      UserClassAdminRepository userClassAdminRepository,
      AdminRepository adminRepository,
      UserRepository userRepository,
      GenericMapper genericMapper,
      ClassService classService,
      Cloudinary cloudinary) {
    this.userClassAdminRepository = userClassAdminRepository;
    this.adminRepository = adminRepository;
    this.userRepository = userRepository;
    this.genericMapper = genericMapper;
    this.classService = classService;
    this.cloudinary = cloudinary;
  }

  @Override
  public Long countAllStudent(StatisticTimeRequest statisticTimeRequest) {
    LocalDate dateStart = null;
    LocalDate dateEnd = null;
    if (statisticTimeRequest != null) {
      dateStart = statisticTimeRequest.getDateStart();
      dateEnd = statisticTimeRequest.getDateEnd();
    }
    return userRepository.countAllByStateNot(dateStart, dateEnd, StateConstant.DELETED.name());
  }

  @Override
  public List<OutputUser> findAll() {
    List<User> listUser =
        userRepository.findAllByStateNot(
            StateConstant.DELETED.name(), Sort.by(Direction.DESC, "createdDate"));
    return genericMapper.mapToListOfType(listUser, OutputUser.class);
  }

  @Override
  public List<OutputUser> findAllTopStudent() {
    List<User> listUser =
        userRepository.findAllByTopStudentAndStateNot(true, StateConstant.DELETED.name());
    return genericMapper.mapToListOfType(listUser, OutputUser.class);
  }

  @Override
  public Page<OutputUser> getListUserPageable(SearchRequest search, int page, int size) {
    Boolean topStudent = null;
    if (Utils.formatParameterQueryEqual(search.getType()) != null) {
      topStudent = Boolean.parseBoolean(Utils.formatParameterQueryEqual(search.getType()));
    }
    Page<User> listUsers =
        userRepository.findAllByNameOrEmailOrPhoneAndStateNot(
            Utils.formatParameterQueryLike(search.getContent()),
            topStudent,
            StateConstant.DELETED.name(),
            PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
    return genericMapper.toPage(
        listUsers,
        OutputUser.class,
        PageRequest.of(page - 1, size, Sort.by(Direction.DESC, "createdDate")));
  }

  @Override
  public OutputUser findById(int id) {
    User user = userRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    return genericMapper.mapToType(user, OutputUser.class);
  }

  @Override
  @SneakyThrows
  @Transactional
  public OutputUser createUser(InputUser data, int adminId) {
    User user =
        userRepository
            .findByEmailAndStateNot(data.getEmail(), StateConstant.DELETED.name())
            .orElse(null);
    if (user == null) {
      user = genericMapper.mapToType(data, User.class);
      if (data.getFile() == null || data.getFile().isEmpty()) {
        user.setImg(ImgConstant.UnknownUser.getValue());
      } else {
        String name = Utils.formatFileName(user.getName()) + "_" + System.currentTimeMillis();
        Transformation incoming =
            new Transformation()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "admin_user/" + name,
                    "transformation",
                    incoming));
        user.setImg(ImgConstant.Prefix.getValue() + "admin_user/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      user.setTopStudent(false);
      user.setState(StateConstant.ACTIVE.name());
      user.setCreatedBy(admin != null ? admin.getName() : "unknown");
      user.setCreatedDate(LocalDate.now());
      user.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      user.setUpdatedDate(LocalDate.now());
      userRepository.save(user);
      return genericMapper.mapToType(user, OutputUser.class);
    }
    return null;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean updateUser(int id, InputUser data, int adminId) {
    User userById =
        userRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    User userByEmail =
        userRepository
            .findUserByIdNotAndEmailAndStateNot(id, data.getEmail(), StateConstant.DELETED.name())
            .orElse(null);
    if (userById != null && userByEmail == null) {
      genericMapper.copyNonNullProperties(data, userById);
      if (data.getFile() != null && !data.getFile().isEmpty()) {
        String name = Utils.formatFileName(data.getEmail()) + "_" + System.currentTimeMillis();
        Transformation incoming =
            new Transformation()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        this.cloudinary
            .uploader()
            .upload(
                data.getFile().getBytes(),
                ObjectUtils.asMap(
                    "resource_type",
                    "auto",
                    "public_id",
                    "admin_user/" + name,
                    "transformation",
                    incoming));
        userById.setImg(ImgConstant.Prefix.getValue() + "admin_user/" + name);
      }
      Admin admin = adminRepository.findById(adminId).orElse(null);
      userById.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      userById.setUpdatedDate(LocalDate.now());
      userRepository.save(userById);
      return true;
    }
    return false;
  }

  @Override
  @SneakyThrows
  @Transactional
  public boolean deleteUser(int id, int adminId) {
    User entity = userRepository.findByIdAndStateNot(id, StateConstant.DELETED.name()).orElse(null);
    if (entity != null) {
      Admin admin = adminRepository.findById(adminId).orElse(null);
      entity.setState(StateConstant.DELETED.name());
      entity.setUpdatedBy(admin != null ? admin.getName() : "unknown");
      entity.setUpdatedDate(LocalDate.now());
      userRepository.save(entity);
      // Xóa tất cả thông tin Học viên trong các Lớp học đã đăng ký
      List<UserClassAdmin> listClass =
          userClassAdminRepository.findAllByStudentIdAndStateNot(
              id, StudentStateConstant.DELETED.name());
      listClass.forEach(
          i -> {
            i.setState(StudentStateConstant.DELETED.name());
            i.setUpdatedBy(admin != null ? admin.getName() : "unknown");
            i.setUpdatedDate(LocalDate.now());
            classService.updateNumberStudent(
                i.getClassCourse().getId(), i.getClassCourse().getNumberStudent() - 1, adminId);
          });
      userClassAdminRepository.saveAll(listClass);
      return true;
    }
    return false;
  }
}
