package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputUser;
import com.example.actwebsite.model.dto.OutputUser;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import org.springframework.data.domain.Page;

public interface UserService {

  /** Đếm tổng số lượng thông tin Học viên */
  Long countAllStudent(StatisticTimeRequest statisticTimeRequest);

  /** Lấy danh sách tất cả thông tin Học viên */
  List<OutputUser> findAll();

  /** Lấy danh sách Top Học viên */
  List<OutputUser> findAllTopStudent();

  /** Lấy danh sách tất cả thông tin Học viên có phân trang và tìm kiếm theo yêu cầu */
  Page<OutputUser> getListUserPageable(SearchRequest search, int page, int size);

  /** Tìm thông tin Học viên theo ID */
  OutputUser findById(int id);

  /** Tạo thông tin Học viên */
  OutputUser createUser(InputUser data, int adminId);

  /** Cập nhật thông tin Học viên */
  boolean updateUser(int id, InputUser data, int adminId);

  /** Xóa thông tin Học viên */
  boolean deleteUser(int id, int adminId);
}
