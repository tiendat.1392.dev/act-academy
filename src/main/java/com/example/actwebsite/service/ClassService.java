package com.example.actwebsite.service;

import com.example.actwebsite.entity.Admin;
import com.example.actwebsite.entity.Course;
import com.example.actwebsite.model.dto.InputClass;
import com.example.actwebsite.model.dto.OutputClass;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;

public interface ClassService {

  /** Lấy danh sách tất cả Lớp học */
  List<OutputClass> findAll();

  /** Lấy danh sách tất cả Lớp học có phân trang và tìm kiếm theo yêu cầu */
  Page<OutputClass> getListClassPageable(SearchRequest search, int page, int size);

  /** Lấy danh sách tất cả Lớp học có phân trang theo ID của Cộng tác viên */
  Page<OutputClass> getListClassPageableByCollaboratorId(int collaboratorId, int page, int size);

  /** Đếm số lượng lớp học theo trạng thái */
  Map<String, Long> countNumberClassByState(StatisticTimeRequest statisticTimeRequest);

  /** Tìm kiếm Lớp học theo ID */
  OutputClass findById(int id);

  /** Tìm kiếm Lớp học dự bị theo ID khóa học */
  OutputClass findPrepClassByCourseId(int courseId);

  /** Tạo lớp học */
  boolean createClass(InputClass data, int adminId);

  /** Tạo lớp học dự bị cho Khóa học */
  void createPrepClass(Course course, Admin admin);

  /** Cập nhật thông tin Lớp học */
  boolean updateClass(int id, InputClass data, int adminId);

  /** Cập nhật số lượng học viên Lớp học */
  void updateNumberStudent(int id, int numberStudent, int adminId);

  /** Xóa lớp học */
  boolean deleteClass(int classId, int adminId);
}
