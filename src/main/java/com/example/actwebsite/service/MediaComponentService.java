package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputMediaComponent;
import com.example.actwebsite.model.request.SliderUpdateRequest;
import java.util.Map;

public interface MediaComponentService {

  String getUrlVideoBanner();

  Map<String, String> getSlider();

  void updateVideoBanner(InputMediaComponent data);

  void updateSlider(SliderUpdateRequest data);
}
