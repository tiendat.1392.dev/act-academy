package com.example.actwebsite.service;

import com.example.actwebsite.model.dto.InputAdmin;
import com.example.actwebsite.model.dto.OutputAdmin;
import com.example.actwebsite.model.request.ChangePasswordRequest;
import com.example.actwebsite.model.request.SearchRequest;
import com.example.actwebsite.model.request.StatisticTimeRequest;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;

public interface AdminService {

  /** Lấy danh sách tất cả Admin */
  List<OutputAdmin> findAll();

  /** Lấy danh sách tất cả Admin có phân trang và tìm kiếm theo yêu cầu */
  Page<OutputAdmin> getListAdminPageable(SearchRequest search, int page, int size);

  /** Đếm số lượng cộng tác viên theo Role */
  Map<String, Long> countNumberCollaboratorByRole(StatisticTimeRequest statisticTimeRequest);

  /** Tìm Admin theo ID */
  OutputAdmin findById(int id);

  /** Tìm Admin theo Email */
  OutputAdmin findByEmail(String email);

  /** Tìm Admin theo Role */
  OutputAdmin findByRole(String role);

  /** Tạo Admin */
  boolean createAdmin(InputAdmin data, int adminId);

  /** Cập nhật thông tin Admin */
  boolean updateAdmin(int id, InputAdmin data, int adminId);

  /** Đổi mật khẩu Admin */
  boolean changePassword(int id, ChangePasswordRequest data);

  /** Xóa Admin */
  boolean deleteAdmin(int id, int adminId);

  /** Cấp mật khẩu mới cho Admin khi quên mật khẩu cũ */
  boolean forgetPassword(String email);

  /** Đổi quản lý học viện */
  boolean changeAdmin(String email, int adminId);
}
