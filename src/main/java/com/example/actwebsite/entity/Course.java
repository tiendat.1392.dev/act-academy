package com.example.actwebsite.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "courses")
public class Course {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Lob
  @Column(name = "img", nullable = false)
  private String img;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Lob
  @Column(name = "`desc`", nullable = false)
  private String desc;

  @Lob
  @Column(name = "content", nullable = false)
  private String content;

  @Column(name = "price", nullable = false)
  private Double price;

  @Column(name = "total_hour_study", nullable = false)
  private Integer totalHourStudy;

  @Lob
  @Column(name = "type", nullable = false)
  private String type;

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private LocalDate createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private LocalDate updatedDate;
}
