package com.example.actwebsite.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "jobs")
public class Job {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Column(name = "password", nullable = false, length = 20)
  private String password;

  @Lob
  @Column(name = "img", nullable = false)
  private String img;

  @Lob
  @Column(name = "name", nullable = false)
  private String name;

  @Lob
  @Column(name = "`desc`", nullable = false)
  private String desc;

  @Column(name = "contact", length = 50)
  private String contact;

  @Column(name = "phone", length = 10)
  private String phone;

  @Column(name = "email", length = 50)
  private String email;

  @Lob
  @Column(name = "address")
  private String address;

  @Lob
  @Column(name = "content", nullable = false)
  private String content;

  @Column(name = "date_posted", nullable = false)
  private LocalDate datePosted;

  @Column(name = "type", nullable = false, length = 45)
  private String type;
}
