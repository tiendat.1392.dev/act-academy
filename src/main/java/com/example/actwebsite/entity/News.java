package com.example.actwebsite.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "news")
public class News {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Lob
  @Column(name = "img", nullable = false)
  private String img;

  @Lob
  @Column(name = "name", nullable = false)
  private String name;

  @Lob
  @Column(name = "`desc`", nullable = false)
  private String desc;

  @Lob
  @Column(name = "text", nullable = false)
  private String text;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "poster_id", nullable = false)
  private Admin poster;

  @Column(name = "type", nullable = false, length = 45)
  private String type;

  @Lob
  @Column(name = "source")
  private String source;

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private LocalDate createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private LocalDate updatedDate;
}
