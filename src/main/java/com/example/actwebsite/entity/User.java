package com.example.actwebsite.entity;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Lob
  @Column(name = "img", nullable = false)
  private String img;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "sex", nullable = false, length = 10)
  private String sex;

  @Column(name = "dob", nullable = false)
  private LocalDate dob;

  @Column(name = "phone", nullable = false, length = 10)
  private String phone;

  @Column(name = "email", nullable = false, length = 50)
  private String email;

  @Lob
  @Column(name = "address")
  private String address;

  @Lob
  @Column(name = "job")
  private String job;

  @Lob
  @Column(name = "workplace")
  private String workplace;

  @Column(name = "top_student", nullable = false)
  private Boolean topStudent = false;

  @Lob
  @Column(name = "feedback")
  private String feedback = "";

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private LocalDate createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private LocalDate updatedDate;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    User user = (User) o;
    return id != null && Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
