package com.example.actwebsite.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "classes")
public class Class {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Lob
  @Column(name = "full_name", nullable = false)
  private String fullName;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "how_to_learn", nullable = false, length = 10)
  private String howToLearn;

  @Column(name = "number_student", nullable = false)
  private Integer numberStudent = 0;

  @Column(name = "date_opening", nullable = false)
  private LocalDate dateOpening;

  @Column(name = "teacher_name", nullable = false, length = 50)
  private String teacherName;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "manager_id", nullable = false)
  @Exclude
  private Admin manager;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "supporter_id", nullable = false)
  @Exclude
  private Admin supporter;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "course_id", nullable = false)
  @Exclude
  private Course course;

  @Lob
  @Column(name = "special")
  private String special;

  @Column(name = "state", nullable = false, length = 10)
  private String state;

  @Column(name = "created_by", nullable = false, length = 50)
  private String createdBy;

  @Column(name = "created_date", nullable = false)
  private LocalDate createdDate;

  @Column(name = "updated_by", nullable = false, length = 50)
  private String updatedBy;

  @Column(name = "updated_date", nullable = false)
  private LocalDate updatedDate;
}
